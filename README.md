# RTP-медиа сервер.
Включает в себя видео-регистратор и rtsp медиа плеер


### Настройка

```sh
"output": {
    "full": {
        "folder": "output/cam1",
        "segmentTime": 30
    }
    "preview": {
        "folder": "output/cam1",
        "segmentTime": 30
    }
}
```
`quality`:
> Если свойство `quality` не задано, то пишется "чистый" поток с камеры.
#### Пример (настройка без перекодирования):
```sh
"output": {
    "preview": {
        "folder": "output/cam1",
        "segmentTime": 30
    },
    ...
}
```
> Если задано свойство `quality`, то поток пишется с перекодированием.
#### Пример (настройка с перекодированием):
```sh
"output": {
    "preview": {
        "folder": "output/cam1",
        "segmentTime": 30,
        "quality": {
            "preset": "ultrafast",
            "bitrate": {
                "min": "500K",
                "max": "900M",
                "buf": "2M",
                "pass": "1"
            }
        }
    },
    ...
}
```

`quality.preset` - Доступны следующие значения: "ultrafast", "superfast", "veryfast", "faster", "fast", "medium", "slow", "slower" и "veryslow".
`quality.bitrate` - Установки качества кодирования для h.264.
`quality.bitrate.min` - Минимальный битрейт (для vbr).
`quality.bitrate.min` - Максимальный битрейт (для vbr).
`quality.bitrate.buf` - Размер буферизации.
`quality.bitrate.pass` - Количество проходов: 1 или 2.

License
---
MIT