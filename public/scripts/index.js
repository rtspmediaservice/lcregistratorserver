var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
define("cross-browser/addEventListener", ["require", "exports"], function (require, exports) {
    "use strict";
    exports.__esModule = true;
    exports.addEventListener = function (obj, type, listener, options) {
        if (obj.addEventListener) {
            obj.addEventListener(type, listener, options);
            return true;
        }
        else if (obj.attachEvent) {
            return obj.attachEvent('on' + type, listener);
        }
        else {
            type = 'on' + type;
            if (typeof obj[type] === 'function') {
                listener = (function (f1, f2) {
                    return function () {
                        f1.apply(this, arguments);
                        f2.apply(this, arguments);
                    };
                })(obj[type], listener);
            }
            obj[type] = listener;
            return true;
        }
    };
});
define("cross-browser/dispatchEvent", ["require", "exports"], function (require, exports) {
    "use strict";
    exports.__esModule = true;
    exports.dispatchEvent = function (target, type, bubbles, cancelable, data) {
        if (bubbles == undefined)
            bubbles = false;
        if (cancelable == undefined)
            cancelable = false;
        var doc = document;
        var event;
        if (document.createEvent) {
            event = doc.createEvent('Event');
            event.data = data;
            event.initEvent(type, bubbles, cancelable);
            target.dispatchEvent(event);
        }
        else {
            event = doc.createEventObject();
            event.data = { data: data };
            target.fireEvent('on' + type, event);
        }
    };
});
define("cross-browser/removeEventListener", ["require", "exports"], function (require, exports) {
    "use strict";
    exports.__esModule = true;
    exports.removeEventListener = function (target, type, listener) {
        if (target.removeEventListener) {
            target.removeEventListener(type, listener);
            return true;
        }
        else if (target.detachEvent) {
            return target.detachEvent('on' + type, listener);
        }
        else {
            type = 'on' + type;
            if (typeof target[type] === 'function') {
                target[type] = null;
                return true;
            }
        }
        return false;
    };
});
define("cross-browser", ["require", "exports", "cross-browser/addEventListener", "cross-browser/dispatchEvent", "cross-browser/removeEventListener"], function (require, exports, addEventListener_1, dispatchEvent_1, removeEventListener_1) {
    "use strict";
    function __export(m) {
        for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
    }
    exports.__esModule = true;
    __export(addEventListener_1);
    __export(dispatchEvent_1);
    __export(removeEventListener_1);
});
define("events/EventUtil", ["require", "exports", "cross-browser"], function (require, exports, cross_browser_1) {
    "use strict";
    exports.__esModule = true;
    var EventUtil = (function () {
        function EventUtil() {
        }
        EventUtil.dispatchEventWith = function (target, type, bubbles, cancelable, data) {
            cross_browser_1.dispatchEvent(target, type, bubbles, cancelable, data);
        };
        EventUtil.removeAllListeners = function (target) {
            var t = target;
            for (var type in t.listeners) {
                delete t.listeners[type];
            }
        };
        return EventUtil;
    }());
    exports.EventUtil = EventUtil;
});
define("controls/BaseControl", ["require", "exports", "events/EventUtil", "cross-browser/addEventListener", "cross-browser/removeEventListener"], function (require, exports, EventUtil_1, addEventListener_2, removeEventListener_2) {
    "use strict";
    exports.__esModule = true;
    var BaseControl = (function () {
        function BaseControl(selectorName) {
            var s = selectorName ? selectorName : 'div';
            this._element = document.createElement(s);
        }
        BaseControl.prototype.get_element = function () {
            return this._element;
        };
        BaseControl.prototype.get_x = function () {
            return this._element.offsetLeft;
        };
        BaseControl.prototype.set_x = function (v) {
            this._element.style.left = v + 'px';
        };
        BaseControl.prototype.get_y = function () {
            return this._element.offsetTop;
        };
        BaseControl.prototype.set_y = function (v) {
            this._element.style.top = v + 'px';
        };
        BaseControl.prototype.get_width = function () {
            return this._element.offsetWidth;
        };
        BaseControl.prototype.set_width = function (v) {
            this._element.style.width = v + 'px';
        };
        BaseControl.prototype.get_height = function () {
            return this._element.offsetHeight;
        };
        BaseControl.prototype.set_height = function (v) {
            this._element.style.height = v + 'px';
        };
        BaseControl.prototype.get_text = function () {
            return this._element.innerHTML;
        };
        BaseControl.prototype.set_text = function (v) {
            this._element.innerHTML = v;
        };
        BaseControl.prototype.get_childrenLength = function () {
            return this._element.children.length;
        };
        BaseControl.setStyleFor = function (element, data) {
            var e = element;
            if (!data || !e || !e.style)
                return;
            for (var k in data) {
                if (e.style.k)
                    e.style[k] = data[k];
            }
        };
        BaseControl.prototype.setStyle = function (data) {
            BaseControl.setStyleFor(this._element, data);
        };
        BaseControl.prototype.contains = function (child) {
            if (!(child instanceof BaseControl)) {
                throw Error("child должен быть унаследован от BaseControl");
            }
            return this._element.contains(child.get_element());
        };
        BaseControl.prototype.appendChild = function (child) {
            if (!(child instanceof BaseControl)) {
                throw Error("child должен быть унаследован от BaseControl");
            }
            this._element.appendChild(child.get_element());
        };
        BaseControl.prototype.appendChildAt = function (child, index) {
            if (!(child instanceof BaseControl)) {
                throw Error("child должен быть унаследован от BaseControl");
            }
            if (!index)
                index = 0;
            if (index >= this._element.children.length)
                this._element.appendChild(child.get_element());
            else
                this._element.insertBefore(child.get_element(), this._element.children[index]);
        };
        BaseControl.prototype.insertAt = function (child, index) {
            if (!(child instanceof BaseControl)) {
                throw Error("child должен быть унаследован от BaseControl");
            }
            if (!index)
                index = 0;
            if (index >= this._element.children.length)
                index = this._element.children.length;
            this._element.insertBefore(child.get_element(), this._element.children[index]);
        };
        BaseControl.prototype.removeChild = function (child) {
            if (!(child instanceof BaseControl)) {
                throw Error("child должен быть унаследован от BaseControl");
            }
            this._element.removeChild(child.get_element());
        };
        BaseControl.prototype.addEventListener = function (type, listener, options) {
            addEventListener_2.addEventListener(this._element, type, listener, options);
        };
        BaseControl.prototype.removeEventListener = function (type, listener) {
            removeEventListener_2.removeEventListener(this._element, type, listener);
        };
        BaseControl.prototype.dispatchEventWith = function (type, bubbles, cancelable, data) {
            if (bubbles === void 0) { bubbles = false; }
            if (cancelable === void 0) { cancelable = true; }
            EventUtil_1.EventUtil.dispatchEventWith(this._element, type, bubbles, cancelable, data);
        };
        BaseControl.prototype.addClass = function (names) {
            this._element.classList.add(names);
        };
        BaseControl.prototype.removeClass = function (names) {
            this._element.classList.remove(names);
        };
        BaseControl.prototype.removeAllListeners = function () {
            EventUtil_1.EventUtil.removeAllListeners(this._element);
        };
        BaseControl.prototype.dispose = function () {
            this.removeAllListeners();
            this._element = null;
        };
        return BaseControl;
    }());
    exports.BaseControl = BaseControl;
});
define("controls/Button", ["require", "exports", "controls/BaseControl"], function (require, exports, BaseControl_1) {
    "use strict";
    exports.__esModule = true;
    var Button = (function (_super) {
        __extends(Button, _super);
        function Button() {
            var _this = _super.call(this, 'div') || this;
            _this.addClass('button');
            _this.$down_handler = function (e) {
                _this.downState();
            };
            _this.addEventListener('mousedown', _this.$down_handler);
            _this.$up_handler = function (e) {
                _this.upState();
            };
            _this.addEventListener('mouseup', _this.$up_handler);
            return _this;
        }
        Button.prototype.get_name = function () { return this._name; };
        Button.prototype.set_name = function (v) { if (this._name != v)
            this._name = v; };
        Button.prototype.upState = function () { };
        Button.prototype.downState = function () { };
        Button.prototype.dispose = function () {
            this.removeEventListener('mousedown', this.$down_handler);
            this.$down_handler = null;
            this.removeEventListener('mouseup', this.$up_handler);
            this.$up_handler = null;
            _super.prototype.dispose.call(this);
        };
        return Button;
    }(BaseControl_1.BaseControl));
    exports.Button = Button;
});
define("controls/AdvancedButton", ["require", "exports", "controls/BaseControl", "controls/Button"], function (require, exports, BaseControl_2, Button_1) {
    "use strict";
    exports.__esModule = true;
    var AdvancedButton = (function (_super) {
        __extends(AdvancedButton, _super);
        function AdvancedButton() {
            var _this = _super.call(this) || this;
            _this._icon = new BaseControl_2.BaseControl();
            _this._icon.addClass('icon');
            _this.appendChild(_this._icon);
            _this._label = new BaseControl_2.BaseControl();
            _this._label.addClass('label');
            _this.appendChild(_this._label);
            return _this;
        }
        AdvancedButton.prototype.get_label = function () { return this._labelStr; };
        AdvancedButton.prototype.set_label = function (v) {
            if (this._labelStr != v) {
                this._labelStr = v;
                this._label.set_text(v);
            }
        };
        AdvancedButton.prototype.get_iconRenderer = function () { return this._icon; };
        AdvancedButton.prototype.get_labelRenderer = function () { return this._label; };
        AdvancedButton.prototype.upState = function () {
            _super.prototype.upState.call(this);
            this._label.removeClass('pressed');
        };
        AdvancedButton.prototype.downState = function () {
            _super.prototype.downState.call(this);
            this._label.addClass('pressed');
        };
        AdvancedButton.prototype.dispose = function () {
            this.removeChild(this._label);
            this._label.dispose();
            this._label = null;
            this.removeChild(this._icon);
            this._icon.dispose();
            this._icon = null;
            _super.prototype.dispose.call(this);
        };
        return AdvancedButton;
    }(Button_1.Button));
    exports.AdvancedButton = AdvancedButton;
});
define("controls/PageNavigator", ["require", "exports", "controls/BaseControl", "controls/AdvancedButton"], function (require, exports, BaseControl_3, AdvancedButton_1) {
    "use strict";
    exports.__esModule = true;
    var PageNavigatorEventTypes;
    (function (PageNavigatorEventTypes) {
        PageNavigatorEventTypes["PREVIOUS_PAGE"] = "page-navigator-previous-page";
        PageNavigatorEventTypes["NEXT_PAGE"] = "page-navigator-next-page";
    })(PageNavigatorEventTypes = exports.PageNavigatorEventTypes || (exports.PageNavigatorEventTypes = {}));
    var PageNavigator = (function (_super) {
        __extends(PageNavigator, _super);
        function PageNavigator() {
            var _this = _super.call(this) || this;
            _this.addClass('page-navigator');
            _this._prevButton = new AdvancedButton_1.AdvancedButton();
            _this.appendChild(_this._prevButton);
            _this._prevButton.get_iconRenderer().addClass('icon-page-navigator__arrow-up');
            _this.$prevButton_handler = function (e) {
                e.stopImmediatePropagation();
                _this.dispatchEventWith(PageNavigatorEventTypes.PREVIOUS_PAGE, true, true);
            };
            _this._prevButton.addEventListener('click', _this.$prevButton_handler);
            _this._prevButton.set_label("пред-я страница");
            _this._indicator = new BaseControl_3.BaseControl();
            _this._indicator.addClass('indicator');
            _this.appendChild(_this._indicator);
            _this._nextButton = new AdvancedButton_1.AdvancedButton();
            _this.appendChild(_this._nextButton);
            _this._nextButton.get_iconRenderer().addClass('icon-page-navigator__arrow-down');
            _this.$nextButton_handler = function (e) {
                e.stopImmediatePropagation();
                _this.dispatchEventWith(PageNavigatorEventTypes.NEXT_PAGE, true, true);
            };
            _this._nextButton.addEventListener('click', _this.$nextButton_handler);
            _this._nextButton.set_label("след-я страница");
            return _this;
        }
        PageNavigator.prototype.setIndicatorText = function (text) {
            this._indicator.get_element().innerHTML = text;
        };
        PageNavigator.prototype.dispose = function () {
            this._prevButton.removeEventListener('click', this.$prevButton_handler);
            this.$prevButton_handler = null;
            this.removeChild(this._prevButton);
            this._prevButton.dispose();
            this._prevButton = null;
            this._nextButton.removeEventListener('click', this.$nextButton_handler);
            this.$nextButton_handler = null;
            this.removeChild(this._nextButton);
            this._nextButton.dispose();
            this._nextButton = null;
            this.removeChild(this._indicator);
            this._indicator.dispose();
            this._indicator = null;
            _super.prototype.dispose.call(this);
        };
        return PageNavigator;
    }(BaseControl_3.BaseControl));
    exports.PageNavigator = PageNavigator;
});
define("controls/InputText", ["require", "exports", "controls/BaseControl"], function (require, exports, BaseControl_4) {
    "use strict";
    exports.__esModule = true;
    var InputText = (function (_super) {
        __extends(InputText, _super);
        function InputText() {
            var _this = _super.call(this, 'input') || this;
            _this._input = _this._element;
            return _this;
        }
        InputText.prototype.get_text = function () {
            return this._input.value;
        };
        InputText.prototype.set_text = function (value) {
            this._input.value = value;
        };
        InputText.prototype.get_placeholder = function () {
            return this._input.placeholder;
        };
        InputText.prototype.set_placeholder = function (value) {
            this._input.placeholder = value;
        };
        InputText.prototype.get_autofocus = function () {
            return this._input.autofocus;
        };
        InputText.prototype.set_autofocus = function (v) {
            this._input.autofocus = v;
        };
        InputText.prototype.dispose = function () {
            this._input = null;
            _super.prototype.dispose.call(this);
        };
        return InputText;
    }(BaseControl_4.BaseControl));
    exports.InputText = InputText;
});
define("controls/BasePopup", ["require", "exports", "controls/BaseControl", "controls/PopupManager"], function (require, exports, BaseControl_5, PopupManager_1) {
    "use strict";
    exports.__esModule = true;
    var BasePopup = (function (_super) {
        __extends(BasePopup, _super);
        function BasePopup() {
            return _super.call(this, 'popup') || this;
        }
        BasePopup.prototype.close = function () {
            this.dispatchEventWith(PopupManager_1.PopupEventTypes.CLOSE, true);
        };
        return BasePopup;
    }(BaseControl_5.BaseControl));
    exports.BasePopup = BasePopup;
});
define("controls/PopupManager", ["require", "exports"], function (require, exports) {
    "use strict";
    exports.__esModule = true;
    var PopupEventTypes;
    (function (PopupEventTypes) {
        PopupEventTypes["CLOSE"] = "popup-close";
    })(PopupEventTypes = exports.PopupEventTypes || (exports.PopupEventTypes = {}));
    var PopupManager = (function () {
        function PopupManager(_name, _root) {
            this._name = _name;
            this._root = _root;
            this._popups = [];
            this._root.addClass("popup-manager");
            if (!PopupManager.popupPool.hasOwnProperty[name])
                PopupManager.popupPool[_name] = this;
        }
        PopupManager.show = function (name, control) {
            if (PopupManager.popupPool[name]) {
                var manager = PopupManager.popupPool[name];
                if (manager)
                    manager.show(control);
            }
        };
        PopupManager.prototype.set_isVisible = function (v) {
            if (this._isVisible != v) {
                this._isVisible = v;
                if (v)
                    this._root.addClass("visible");
                else
                    this._root.removeClass("visible");
            }
        };
        PopupManager.prototype.show = function (control) {
            var _this = this;
            var $popupClose_handler = function (e) {
                control.removeEventListener(PopupEventTypes.CLOSE, $popupClose_handler);
                _this.removePopup(control);
            };
            control.addEventListener(PopupEventTypes.CLOSE, $popupClose_handler);
            this._popups.push(control);
            this._root.appendChild(control);
            this.set_isVisible(true);
        };
        PopupManager.prototype.removePopup = function (popup) {
            var index = this._popups.indexOf(popup);
            this._popups.splice(index, 1);
            this._root.removeChild(popup);
            popup.dispose();
            popup = null;
            if (this._popups.length == 0)
                this.set_isVisible(false);
        };
        PopupManager.prototype.removePopups = function () {
            while (this._popups.length > 0) {
                var popup = this._popups.pop();
                this.removePopup(popup);
            }
            this.set_isVisible(false);
        };
        PopupManager.prototype.dispose = function () {
            this.removePopups();
            delete PopupManager.popupPool[name];
            this._root = null;
            this._popups = null;
        };
        PopupManager.popupPool = {};
        return PopupManager;
    }());
    exports.PopupManager = PopupManager;
});
define("controls/SimpleForm", ["require", "exports", "controls/BaseControl"], function (require, exports, BaseControl_6) {
    "use strict";
    exports.__esModule = true;
    var SimpleForm = (function (_super) {
        __extends(SimpleForm, _super);
        function SimpleForm() {
            var _this = _super.call(this, 'form') || this;
            _this._form = _this._element;
            return _this;
        }
        SimpleForm.prototype.get_name = function () {
            return this._form.name;
        };
        SimpleForm.prototype.set_name = function (v) {
            this._form.name = v;
        };
        SimpleForm.prototype.dispose = function () {
            this._form = null;
            _super.prototype.dispose.call(this);
        };
        return SimpleForm;
    }(BaseControl_6.BaseControl));
    exports.SimpleForm = SimpleForm;
});
define("controls/Alert", ["require", "exports", "controls/BasePopup", "controls/InputText", "controls"], function (require, exports, BasePopup_1, InputText_1, controls_1) {
    "use strict";
    exports.__esModule = true;
    var AlertContentType;
    (function (AlertContentType) {
        AlertContentType[AlertContentType["MESSAGE"] = 0] = "MESSAGE";
        AlertContentType[AlertContentType["INPUT"] = 1] = "INPUT";
    })(AlertContentType = exports.AlertContentType || (exports.AlertContentType = {}));
    var Alert = (function (_super) {
        __extends(Alert, _super);
        function Alert(data) {
            var _this = _super.call(this) || this;
            _this._buttons = [];
            _this.addClass("alert");
            _this._form = new controls_1.SimpleForm();
            _this._form.set_name("alert");
            if (data) {
                if (data["class"])
                    _this.addClass(data["class"]);
                if (data.header) {
                    _this._header = new controls_1.BaseControl();
                    _this._header.set_text(data.header);
                    _this._header.addClass('header');
                    _this._form.appendChild(_this._header);
                }
            }
            _this._content = new controls_1.BaseControl();
            _this._content.addClass('content');
            _this._form.appendChild(_this._content);
            if (data) {
                if (data.contentType) {
                    switch (data.contentType) {
                        case AlertContentType.MESSAGE: {
                            if (data.message)
                                _this._content.set_text(data.message);
                            break;
                        }
                        case AlertContentType.INPUT: {
                            _this._input = new InputText_1.InputText();
                            _this._input.addClass('input');
                            if (data.input) {
                                _this._input.set_autofocus(Boolean(data.input.autofocus));
                                if (data.input.text)
                                    _this._input.set_text(data.input.text);
                                if (data.input.placeholder)
                                    _this._input.set_text(data.input.placeholder);
                            }
                            _this._content.appendChild(_this._input);
                            break;
                        }
                    }
                }
                if (data.buttonsCollection) {
                    _this._footer = new controls_1.BaseControl();
                    _this._footer.addClass('footer');
                    _this._form.appendChild(_this._footer);
                    _this.createButtons(data);
                }
            }
            _this.appendChild(_this._form);
            return _this;
        }
        Alert.prototype.get_inputValue = function () {
            return this._input ? this._input.get_text() : undefined;
        };
        Alert.prototype.get_header = function () { return this._headerStr; };
        Alert.prototype.set_header = function (v) {
            if (this._headerStr != v) {
                this._headerStr = v;
                this._header.set_text(v);
            }
        };
        Alert.prototype.createButtons = function (data) {
            var _this = this;
            if (data.buttonsCollection) {
                var _loop_1 = function (buttonData) {
                    var button = new controls_1.Button();
                    if (buttonData["class"])
                        button.addClass(buttonData["class"]);
                    if (buttonData.label)
                        button.set_text(buttonData.label);
                    if (data.trigger) {
                        button.addEventListener('click', function (e) {
                            data.trigger(buttonData, _this);
                        });
                    }
                    this_1._footer.appendChild(button);
                    this_1._buttons.push(button);
                };
                var this_1 = this;
                for (var _i = 0, _a = data.buttonsCollection; _i < _a.length; _i++) {
                    var buttonData = _a[_i];
                    _loop_1(buttonData);
                }
            }
        };
        Alert.prototype.removeButtons = function () {
            while (this._buttons.length > 0) {
                var button = this._buttons.pop();
                button.removeAllListeners();
                this._footer.removeChild(button);
                button.dispose();
                button = null;
            }
        };
        Alert.prototype.close = function () {
            this.dispatchEventWith(controls_1.PopupEventTypes.CLOSE, true);
        };
        Alert.prototype.dispose = function () {
            this.removeButtons();
            _super.prototype.dispose.call(this);
        };
        return Alert;
    }(BasePopup_1.BasePopup));
    exports.Alert = Alert;
});
define("controls", ["require", "exports", "controls/AdvancedButton", "controls/BaseControl", "controls/Button", "controls/PageNavigator", "controls/InputText", "controls/PopupManager", "controls/SimpleForm", "controls/Alert"], function (require, exports, AdvancedButton_2, BaseControl_7, Button_2, PageNavigator_1, InputText_2, PopupManager_2, SimpleForm_1, Alert_1) {
    "use strict";
    function __export(m) {
        for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
    }
    exports.__esModule = true;
    __export(AdvancedButton_2);
    __export(BaseControl_7);
    __export(Button_2);
    __export(PageNavigator_1);
    __export(InputText_2);
    __export(PopupManager_2);
    __export(SimpleForm_1);
    __export(Alert_1);
});
define("events/EventDispatcher", ["require", "exports", "events/Event"], function (require, exports, Event_1) {
    "use strict";
    exports.__esModule = true;
    var EventDispatcher = (function () {
        function EventDispatcher() {
            this._eventStack = [];
        }
        EventDispatcher.prototype.addEventListener = function (type, listener) {
            if (this._eventListeners == null)
                this._eventListeners = {};
            var listeners = this._eventListeners[type];
            if (listeners == null)
                this._eventListeners[type] = [listener];
            else if (listeners.indexOf(listener) == -1)
                listeners[listeners.length] = listener;
        };
        EventDispatcher.prototype.removeEventListener = function (type, listener) {
            if (this._eventListeners) {
                var listeners = this._eventListeners[type];
                var numListeners = listeners ? listeners.length : 0;
                if (numListeners > 0) {
                    var index = listeners.indexOf(listener);
                    if (index != -1) {
                        if (this._eventStack.indexOf(type) == -1) {
                            listeners.splice(index, 1);
                        }
                        else {
                            var restListeners = listeners.slice(0, index);
                            for (var i = index + 1; i < numListeners; ++i) {
                                restListeners[i - 1] = listeners[i];
                            }
                            this._eventListeners[type] = restListeners;
                        }
                    }
                }
            }
        };
        EventDispatcher.prototype.removeEventListeners = function (type) {
            if (type && this._eventListeners)
                delete this._eventListeners[type];
            else
                this._eventListeners = null;
        };
        EventDispatcher.prototype.dispatchEvent = function (event) {
            var bubbles = event.get_bubbles();
            if (!bubbles && (this._eventListeners == null || !this._eventListeners.hasOwnProperty(event.get_type())))
                return;
            var previousTarget = event.get_target();
            event.setTarget(this);
            this.invokeEvent(event);
            if (previousTarget)
                event.setTarget(previousTarget);
        };
        EventDispatcher.prototype.invokeEvent = function (event) {
            var listeners = this._eventListeners ? this._eventListeners[event.get_type()] : null;
            var numListeners = listeners == null ? 0 : listeners.length;
            if (numListeners) {
                event.setCurrentTarget(this);
                this._eventStack[this._eventStack.length] = event.get_type();
                for (var i = 0; i < numListeners; ++i) {
                    var listener = listeners[i];
                    var numArgs = listener.length;
                    if (numArgs == 0)
                        listener();
                    else if (numArgs == 1)
                        listener(event);
                    else
                        listener(event, event.get_data());
                    if (event.get_stopsImmediatePropagation())
                        return true;
                }
                this._eventStack.pop();
                return event.get_stopsPropagation();
            }
            else {
                return false;
            }
        };
        EventDispatcher.prototype.dispatchEventWith = function (type, bubbles, data) {
            if (bubbles === void 0) { bubbles = false; }
            if (data === void 0) { data = null; }
            if (bubbles || this.hasEventListener(type)) {
                var event = Event_1.Event.fromPool(type, bubbles, data);
                this.dispatchEvent(event);
                Event_1.Event.toPool(event);
            }
        };
        EventDispatcher.prototype.hasEventListener = function (type, listener) {
            var listeners = this._eventListeners ? this._eventListeners[type] : null;
            if (listeners == null)
                return false;
            else {
                if (listener != null)
                    return listeners.indexOf(listener) != -1;
                else
                    return listeners.length != 0;
            }
        };
        EventDispatcher.prototype.dispose = function () {
            this.removeEventListeners();
        };
        return EventDispatcher;
    }());
    exports.EventDispatcher = EventDispatcher;
});
define("events/Event", ["require", "exports"], function (require, exports) {
    "use strict";
    exports.__esModule = true;
    var Event = (function () {
        function Event(_type, _bubbles, _data) {
            if (_bubbles === void 0) { _bubbles = false; }
            this._type = _type;
            this._bubbles = _bubbles;
            this._data = _data;
        }
        Event.prototype.stopPropagation = function () {
            this._stopsPropagation = true;
        };
        Event.prototype.stopImmediatePropagation = function () {
            this._stopsPropagation = this._stopsImmediatePropagation = true;
        };
        Event.prototype.get_bubbles = function () { return this._bubbles; };
        Event.prototype.get_target = function () {
            return this._target;
        };
        Event.prototype.get_currentTarget = function () {
            return this._currentTarget;
        };
        Event.prototype.get_type = function () {
            return this._type;
        };
        Event.prototype.get_data = function () {
            return this._data;
        };
        Event.prototype.setTarget = function (value) {
            this._target = value;
        };
        Event.prototype.setCurrentTarget = function (value) {
            this._currentTarget = value;
        };
        Event.prototype.setData = function (value) {
            this._data = value;
        };
        Event.prototype.get_stopsPropagation = function () {
            return this._stopsPropagation;
        };
        Event.prototype.get_stopsImmediatePropagation = function () {
            return this._stopsImmediatePropagation;
        };
        Event.fromPool = function (type, bubbles, data) {
            if (bubbles === void 0) { bubbles = false; }
            if (Event.eventPool.length > 0)
                return Event.eventPool.pop().reset(type, bubbles, data);
            else
                return new Event(type, bubbles, data);
        };
        Event.toPool = function (event) {
            event._data = event._target = event._currentTarget = null;
            Event.eventPool[Event.eventPool.length] = event;
        };
        Event.prototype.reset = function (type, bubbles, data) {
            if (bubbles === void 0) { bubbles = false; }
            if (data === void 0) { data = null; }
            this._type = type;
            this._bubbles = bubbles;
            this._data = data;
            this._target = this._currentTarget = null;
            this._stopsPropagation = this._stopsImmediatePropagation = false;
            return this;
        };
        Event.FAIL = 'fail';
        Event.PROGRESS = 'progress';
        Event.eventPool = [];
        return Event;
    }());
    exports.Event = Event;
});
define("events", ["require", "exports", "events/Event", "events/EventDispatcher", "events/EventUtil"], function (require, exports, Event_2, EventDispatcher_1, EventUtil_2) {
    "use strict";
    function __export(m) {
        for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
    }
    exports.__esModule = true;
    __export(Event_2);
    __export(EventDispatcher_1);
    __export(EventUtil_2);
});
define("data/DataLoader", ["require", "exports", "events"], function (require, exports, events_1) {
    "use strict";
    exports.__esModule = true;
    var DataLoaderContentType;
    (function (DataLoaderContentType) {
        DataLoaderContentType[DataLoaderContentType["JSON"] = 0] = "JSON";
        DataLoaderContentType[DataLoaderContentType["TEXT"] = 1] = "TEXT";
    })(DataLoaderContentType = exports.DataLoaderContentType || (exports.DataLoaderContentType = {}));
    var DataLoaderEventTypes;
    (function (DataLoaderEventTypes) {
        DataLoaderEventTypes["DATA"] = "data";
        DataLoaderEventTypes["ERROR"] = "error";
    })(DataLoaderEventTypes = exports.DataLoaderEventTypes || (exports.DataLoaderEventTypes = {}));
    var DataLoader = (function (_super) {
        __extends(DataLoader, _super);
        function DataLoader(_token) {
            var _this = _super.call(this) || this;
            _this._token = _token;
            _this._isLoading = false;
            _this._isComplete = false;
            _this._autoLoad = false;
            return _this;
        }
        DataLoader.prototype.get_content = function () { return this._content; };
        DataLoader.prototype.get_autoLoad = function () { return this._autoLoad; };
        DataLoader.prototype.set_autoLoad = function (v) {
            if (this._autoLoad != v) {
                if (!this._isLoading && !this._isComplete && this._source)
                    this.load();
            }
        };
        DataLoader.prototype.get_source = function () { return this._source; };
        DataLoader.prototype.set_source = function (v) {
            if (this._source != v) {
                this._source = v;
                if (this._source && this._autoLoad)
                    this.load();
            }
        };
        DataLoader.prototype.get_contentType = function () { return this._contentType; };
        DataLoader.prototype.set_contentType = function (v) {
            if (this._contentType != v) {
                this._contentType = v;
                this.transformContentIfNeed();
            }
        };
        DataLoader.prototype.set_params = function (params) {
            this._params = params;
        };
        DataLoader.prototype.load = function () {
            var _this = this;
            this._isLoading = true;
            this._isComplete = false;
            if (this._request)
                this.unload();
            this._request = new XMLHttpRequest();
            this._request.onreadystatechange = function () {
                if (_this._request.readyState == 4 && _this._request.status == 200) {
                    _this._response = _this._request.responseText;
                    var error = false;
                    try {
                        _this.transformContentIfNeed();
                    }
                    catch (e) {
                        error = e;
                    }
                    if (error)
                        _this.dispatchEventWith.call(_this, DataLoaderEventTypes.ERROR, false, error);
                    _this.dispatchEventWith.call(_this, DataLoaderEventTypes.DATA, false, _this._content);
                    if (_this._request)
                        _this.unload();
                    _this._isComplete = true;
                }
            };
            var queryParams = this.getQueryParamsAsString();
            this._request.open("GET", this._source + queryParams, true);
            if (this._token)
                this._request.setRequestHeader('token', this._token);
            this._request.send();
        };
        DataLoader.prototype.getQueryParamsAsString = function () {
            var result = "";
            if (this._params) {
                for (var i = 0, l = this._params.length; i < l; i++) {
                    var param = this._params[i];
                    if (i == 0)
                        result += "?";
                    result += param.key + "=" + String(param.value);
                    if (i < l - 1)
                        result += "&";
                }
            }
            return result;
        };
        DataLoader.prototype.transformContentIfNeed = function () {
            switch (this._contentType) {
                case DataLoaderContentType.JSON:
                    if (this._response)
                        this._content = JSON.parse(this._response);
                    break;
                default:
                    this._content = this._response;
            }
        };
        DataLoader.prototype.unload = function () {
            this._isLoading = false;
            this._request.onreadystatechange = null;
            this._request.abort();
            this._request = null;
            this._content = null;
        };
        DataLoader.prototype.dispose = function () {
            this.removeEventListeners();
            this._response = null;
            if (this._request)
                this.unload();
            this._params = null;
            _super.prototype.dispose.call(this);
        };
        return DataLoader;
    }(events_1.EventDispatcher));
    exports.DataLoader = DataLoader;
});
define("data/DynamicCSS", ["require", "exports"], function (require, exports) {
    "use strict";
    exports.__esModule = true;
    var DynamicCSS = (function () {
        function DynamicCSS(filename, cb) {
            this._ref = document.createElement("link");
            this._ref.setAttribute("rel", "stylesheet");
            this._ref.setAttribute("type", "text/css");
            this._ref.setAttribute("href", filename);
            this._ref.onload = function () {
                cb();
            };
            document.getElementsByTagName('head')[0].appendChild(this._ref);
        }
        return DynamicCSS;
    }());
    exports.DynamicCSS = DynamicCSS;
});
define("data", ["require", "exports", "data/DataLoader", "data/DynamicCSS"], function (require, exports, DataLoader_1, DynamicCSS_1) {
    "use strict";
    function __export(m) {
        for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
    }
    exports.__esModule = true;
    __export(DataLoader_1);
    __export(DynamicCSS_1);
});
define("core/HttpService", ["require", "exports", "data", "events"], function (require, exports, data_1, events_2) {
    "use strict";
    exports.__esModule = true;
    var HttpService = (function (_super) {
        __extends(HttpService, _super);
        function HttpService() {
            var _this = _super.call(this) || this;
            _this._count = 0;
            _this._loaders = [];
            return _this;
        }
        HttpService.prototype.get_token = function () {
            return "1";
        };
        HttpService.prototype.getCamList = function (cb) {
            this.loadJSONRes('api/camlist', null, cb);
        };
        HttpService.prototype.getCamPreview = function (camName, cb) {
            this.loadJSONRes('api/campreview', [{ key: "cam", value: camName }], cb);
        };
        HttpService.prototype.loadJSONRes = function (resource, params, cb) {
            var _this = this;
            var dataLoader = new data_1.DataLoader(this.get_token());
            dataLoader.set_contentType(data_1.DataLoaderContentType.JSON);
            var $data_handler = function (e) {
                cb(e.get_data());
                _this.removeLoader(dataLoader);
            };
            var $error_handler = function (e) {
                console.log(e);
                _this.removeLoader(dataLoader);
            };
            if (params)
                dataLoader.set_params(params);
            dataLoader.addEventListener(data_1.DataLoaderEventTypes.DATA, $data_handler);
            dataLoader.addEventListener(data_1.DataLoaderEventTypes.ERROR, $error_handler);
            this._loaders.push(dataLoader);
            dataLoader.set_source(resource);
            dataLoader.load();
        };
        HttpService.prototype.removeLoader = function (dataLoader) {
            var index = this._loaders.indexOf(dataLoader);
            if (index > -1) {
                this._loaders.splice(index, 1);
                dataLoader.dispose();
                dataLoader = null;
            }
        };
        HttpService.prototype.removeAllLoaders = function () {
            for (var _i = 0, _a = this._loaders; _i < _a.length; _i++) {
                var dataLoader = _a[_i];
                dataLoader.dispose();
                dataLoader = null;
            }
            this._loaders = null;
        };
        HttpService.prototype.dispose = function () {
            this.removeAllLoaders();
            _super.prototype.dispose.call(this);
        };
        return HttpService;
    }(events_2.EventDispatcher));
    exports.HttpService = HttpService;
});
define("models/ISettingsModel", ["require", "exports"], function (require, exports) {
    "use strict";
    exports.__esModule = true;
});
define("models", ["require", "exports"], function (require, exports) {
    "use strict";
    exports.__esModule = true;
});
define("core/Settings", ["require", "exports", "events"], function (require, exports, events_3) {
    "use strict";
    exports.__esModule = true;
    var SettingsEventTypes;
    (function (SettingsEventTypes) {
        SettingsEventTypes["COMPLETE"] = "settings-complete";
    })(SettingsEventTypes = exports.SettingsEventTypes || (exports.SettingsEventTypes = {}));
    var Settings = (function (_super) {
        __extends(Settings, _super);
        function Settings(_data) {
            var _this = _super.call(this) || this;
            _this._data = _data;
            Settings.instance = _this;
            return _this;
        }
        Settings.prototype.get_data = function () { return this._data; };
        Settings.prototype.dispose = function () {
            this._date = null;
            _super.prototype.dispose.call(this);
        };
        Settings.demo = true;
        return Settings;
    }(events_3.EventDispatcher));
    exports.Settings = Settings;
});
define("core/SocketService", ["require", "exports"], function (require, exports) {
    "use strict";
    exports.__esModule = true;
    var WSEventTypes;
    (function (WSEventTypes) {
        WSEventTypes["OPEN"] = "open";
        WSEventTypes["CLOSE"] = "close";
        WSEventTypes["MESSAGE"] = "message";
        WSEventTypes["ERROR"] = "error";
    })(WSEventTypes || (WSEventTypes = {}));
    var Commands;
    (function (Commands) {
        Commands["PING"] = "PING";
        Commands["UPDATE"] = "UPDATE";
        Commands["LOGOUT"] = "LOGOUT";
    })(Commands || (Commands = {}));
    var SocketService = (function () {
        function SocketService(_context, _onMessage, _onOpen, _onClose, _onError) {
            this._context = _context;
            this._onMessage = _onMessage;
            this._onOpen = _onOpen;
            this._onClose = _onClose;
            this._onError = _onError;
            this.createSocket();
        }
        SocketService.prototype.createSocket = function () {
            var _this = this;
            this._socket = new WebSocket("ws://" + location.host + "/", []);
            this.$socket_openHandler = function (e) {
                if (_this._onOpen)
                    _this._onOpen.call(_this._context, e);
            };
            this._socket.addEventListener(WSEventTypes.OPEN, this.$socket_openHandler);
            this.$socket_closeHandler = function (e) {
                if (_this._onClose)
                    _this._onClose.call(_this._context, e);
            };
            this._socket.addEventListener(WSEventTypes.CLOSE, this.$socket_closeHandler);
            this.$socket_errorHandler = function (e) {
                console.log("Ошибка на сокете", e);
                if (_this._onError)
                    _this._onError.call(_this._context, e);
            };
            this._socket.addEventListener(WSEventTypes.ERROR, this.$socket_errorHandler);
            this.$socket_messageHandler = function (e) {
                if (_this._onMessage)
                    _this._onMessage.call(_this._context, e);
            };
            this._socket.addEventListener(WSEventTypes.MESSAGE, this.$socket_messageHandler);
        };
        SocketService.prototype.reconnect = function () {
            this.removeSocket();
            this.createSocket();
        };
        SocketService.prototype.removeSocket = function () {
            if (this._socket) {
                this._socket.removeEventListener(WSEventTypes.OPEN, this.$socket_openHandler);
                this._socket.removeEventListener(WSEventTypes.CLOSE, this.$socket_closeHandler);
                this._socket.removeEventListener(WSEventTypes.ERROR, this.$socket_errorHandler);
                this._socket.removeEventListener(WSEventTypes.MESSAGE, this.$socket_messageHandler);
                this._socket.close();
                this._socket = null;
            }
        };
        SocketService.prototype.send = function (message) {
            try {
                if (this._socket)
                    this._socket.send(message);
            }
            catch (e) {
                if (this._onError)
                    this._onError.call(this._context, e);
            }
        };
        SocketService.prototype.dispose = function () {
            this.removeSocket();
            this._onClose = null;
            this._onError = null;
            this._onOpen = null;
            this._onMessage = null;
        };
        return SocketService;
    }());
    exports.SocketService = SocketService;
});
define("core", ["require", "exports", "core/HttpService", "core/Settings", "core/SocketService"], function (require, exports, HttpService_1, Settings_1, SocketService_1) {
    "use strict";
    function __export(m) {
        for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
    }
    exports.__esModule = true;
    __export(HttpService_1);
    __export(Settings_1);
    __export(SocketService_1);
});
define("types/PopupTypes", ["require", "exports"], function (require, exports) {
    "use strict";
    exports.__esModule = true;
    var PopupTypes;
    (function (PopupTypes) {
        PopupTypes["MAIN"] = "main";
    })(PopupTypes = exports.PopupTypes || (exports.PopupTypes = {}));
});
define("types", ["require", "exports", "types/PopupTypes"], function (require, exports, PopupTypes_1) {
    "use strict";
    function __export(m) {
        for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
    }
    exports.__esModule = true;
    __export(PopupTypes_1);
});
define("controls/CanvasPlayer", ["require", "exports", "controls/BaseControl"], function (require, exports, BaseControl_8) {
    "use strict";
    exports.__esModule = true;
    var CanvasPlayer = (function (_super) {
        __extends(CanvasPlayer, _super);
        function CanvasPlayer() {
            var _this = _super.call(this, "canvas") || this;
            _this._canvas = _this._element;
            _this._canvas.width = 320;
            _this._canvas.height = 240;
            _this._context = _this._canvas.getContext("2d");
            _this._image = document.createElement("img");
            var ctx = _this._context;
            _this._image.onload = function () {
                if (ctx)
                    ctx.drawImage(_this._image, 0, 0, 320, 240);
            };
            return _this;
        }
        CanvasPlayer.prototype.uploadImage = function (base64Img) {
            if (this._image)
                this._image.src = base64Img;
        };
        CanvasPlayer.prototype.dispose = function () {
            if (this._image) {
                this._image.onload = null;
                this._image.src = null;
                this._image = null;
            }
            this._context = null;
            this._canvas = null;
            _super.prototype.dispose.call(this);
        };
        return CanvasPlayer;
    }(BaseControl_8.BaseControl));
    exports.CanvasPlayer = CanvasPlayer;
});
define("controls/RealtimePlayer", ["require", "exports", "controls"], function (require, exports, controls_2) {
    "use strict";
    exports.__esModule = true;
    var RealtimePlayer = (function (_super) {
        __extends(RealtimePlayer, _super);
        function RealtimePlayer(_httpService, _cameraName) {
            var _this = _super.call(this) || this;
            _this._httpService = _httpService;
            _this._cameraName = _cameraName;
            _this.addClass("realtime-player");
            _this._video = document.createElement("video");
            _this._element.appendChild(_this._video);
            if (Hls.isSupported()) {
                var hls = new Hls();
                hls.loadSource("http://127.0.0.1:3567/output/index.m3u8");
                hls.attachMedia(_this._video);
                hls.on(Hls.Events.MANIFEST_PARSED, function () {
                    _this._video.play();
                });
            }
            return _this;
        }
        RealtimePlayer.prototype.play = function () {
        };
        RealtimePlayer.prototype.stop = function () {
        };
        RealtimePlayer.prototype.execCmd = function (data) {
        };
        RealtimePlayer.prototype.uploadSnapshot = function (data) {
        };
        RealtimePlayer.prototype.dispose = function () {
            _super.prototype.dispose.call(this);
        };
        return RealtimePlayer;
    }(controls_2.BaseControl));
    exports.RealtimePlayer = RealtimePlayer;
});
define("Main", ["require", "exports", "controls", "core", "cross-browser", "types", "controls/RealtimePlayer"], function (require, exports, controls_3, core_1, cross_browser_2, types_1, RealtimePlayer_1) {
    "use strict";
    exports.__esModule = true;
    var Main = (function (_super) {
        __extends(Main, _super);
        function Main() {
            var _this = _super.call(this) || this;
            _this._isInitialized = false;
            _this._lastTimestamp = 0;
            _this._realtimePlayers = [];
            _this.addClass("main");
            _this._stage = new controls_3.BaseControl();
            _this._stage.addClass('stage');
            _this.appendChild(_this._stage);
            _this._popupStage = new controls_3.BaseControl();
            _this._popupStage.addClass('popup-stage');
            _this.appendChild(_this._popupStage);
            document.getElementsByTagName('body')[0].appendChild(_this._element);
            new controls_3.PopupManager(types_1.PopupTypes.MAIN, _this._popupStage);
            _this._service = new core_1.HttpService();
            _this.createPlayers();
            return _this;
        }
        Main.prototype.createPlayers = function () {
            var _this = this;
            this._service.getCamList(function (resp) {
                var camList = resp;
                if (camList) {
                    for (var _i = 0, camList_1 = camList; _i < camList_1.length; _i++) {
                        var cam = camList_1[_i];
                        var player = new RealtimePlayer_1.RealtimePlayer(_this._service, cam);
                        player.addClass("player");
                        _this._stage.appendChild(player);
                        _this._realtimePlayers.push(player);
                        player.play();
                    }
                }
            });
        };
        Main.prototype.runTimer = function () {
            var _this = this;
            this.tick();
            setTimeout(function () {
                _this.runTimer();
            }, 1000);
        };
        Main.prototype.tick = function () {
            var dt = new Date();
        };
        Main.prototype.initialize = function () {
        };
        Main.prototype.dispose = function () {
            document.getElementsByTagName('body')[0].removeChild(this._element);
            this._element = null;
            cross_browser_2.removeEventListener(window, 'resize', this.$resizeHandler);
            this.$resizeHandler = null;
            _super.prototype.dispose.call(this);
        };
        return Main;
    }(controls_3.BaseControl));
    exports.Main = Main;
});
define("index", ["require", "exports", "Main"], function (require, exports, Main_1) {
    "use strict";
    exports.__esModule = true;
    new Main_1.Main();
});
