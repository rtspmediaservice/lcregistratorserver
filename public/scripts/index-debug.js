var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
define("cross-browser/addEventListener", ["require", "exports"], function (require, exports) {
    "use strict";
    exports.__esModule = true;
    exports.addEventListener = function (obj, type, listener, options) {
        if (obj.addEventListener) {
            obj.addEventListener(type, listener, options);
            return true;
        }
        else if (obj.attachEvent) {
            return obj.attachEvent('on' + type, listener);
        }
        else {
            type = 'on' + type;
            if (typeof obj[type] === 'function') {
                listener = (function (f1, f2) {
                    return function () {
                        f1.apply(this, arguments);
                        f2.apply(this, arguments);
                    };
                })(obj[type], listener);
            }
            obj[type] = listener;
            return true;
        }
    };
});
define("cross-browser/dispatchEvent", ["require", "exports"], function (require, exports) {
    "use strict";
    exports.__esModule = true;
    exports.dispatchEvent = function (target, type, bubbles, cancelable, data) {
        if (bubbles == undefined)
            bubbles = false;
        if (cancelable == undefined)
            cancelable = false;
        var doc = document;
        var event;
        if (document.createEvent) {
            event = doc.createEvent('Event');
            event.data = data;
            event.initEvent(type, bubbles, cancelable);
            target.dispatchEvent(event);
        }
        else {
            event = doc.createEventObject();
            event.data = { data: data };
            target.fireEvent('on' + type, event);
        }
    };
});
define("cross-browser/removeEventListener", ["require", "exports"], function (require, exports) {
    "use strict";
    exports.__esModule = true;
    exports.removeEventListener = function (target, type, listener) {
        if (target.removeEventListener) {
            target.removeEventListener(type, listener);
            return true;
        }
        else if (target.detachEvent) {
            return target.detachEvent('on' + type, listener);
        }
        else {
            type = 'on' + type;
            if (typeof target[type] === 'function') {
                target[type] = null;
                return true;
            }
        }
        return false;
    };
});
define("cross-browser", ["require", "exports", "cross-browser/addEventListener", "cross-browser/dispatchEvent", "cross-browser/removeEventListener"], function (require, exports, addEventListener_1, dispatchEvent_1, removeEventListener_1) {
    "use strict";
    function __export(m) {
        for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
    }
    exports.__esModule = true;
    __export(addEventListener_1);
    __export(dispatchEvent_1);
    __export(removeEventListener_1);
});
define("events/EventUtil", ["require", "exports", "cross-browser"], function (require, exports, cross_browser_1) {
    "use strict";
    exports.__esModule = true;
    var EventUtil = (function () {
        function EventUtil() {
        }
        EventUtil.dispatchEventWith = function (target, type, bubbles, cancelable, data) {
            cross_browser_1.dispatchEvent(target, type, bubbles, cancelable, data);
        };
        EventUtil.removeAllListeners = function (target) {
            var t = target;
            for (var type in t.listeners) {
                delete t.listeners[type];
            }
        };
        return EventUtil;
    }());
    exports.EventUtil = EventUtil;
});
define("controls/BaseControl", ["require", "exports", "events/EventUtil", "cross-browser/addEventListener", "cross-browser/removeEventListener"], function (require, exports, EventUtil_1, addEventListener_2, removeEventListener_2) {
    "use strict";
    exports.__esModule = true;
    var BaseControl = (function () {
        function BaseControl(selectorName) {
            var s = selectorName ? selectorName : 'div';
            this._element = document.createElement(s);
        }
        BaseControl.prototype.get_element = function () {
            return this._element;
        };
        BaseControl.prototype.get_x = function () {
            return this._element.offsetLeft;
        };
        BaseControl.prototype.set_x = function (v) {
            this._element.style.left = v + 'px';
        };
        BaseControl.prototype.get_y = function () {
            return this._element.offsetTop;
        };
        BaseControl.prototype.set_y = function (v) {
            this._element.style.top = v + 'px';
        };
        BaseControl.prototype.get_width = function () {
            return this._element.offsetWidth;
        };
        BaseControl.prototype.set_width = function (v) {
            this._element.style.width = v + 'px';
        };
        BaseControl.prototype.get_height = function () {
            return this._element.offsetHeight;
        };
        BaseControl.prototype.set_height = function (v) {
            this._element.style.height = v + 'px';
        };
        BaseControl.prototype.get_text = function () {
            return this._element.innerHTML;
        };
        BaseControl.prototype.set_text = function (v) {
            this._element.innerHTML = v;
        };
        BaseControl.prototype.get_childrenLength = function () {
            return this._element.children.length;
        };
        BaseControl.setStyleFor = function (element, data) {
            var e = element;
            if (!data || !e || !e.style)
                return;
            for (var k in data) {
                if (e.style.k)
                    e.style[k] = data[k];
            }
        };
        BaseControl.prototype.setStyle = function (data) {
            BaseControl.setStyleFor(this._element, data);
        };
        BaseControl.prototype.contains = function (child) {
            if (!(child instanceof BaseControl)) {
                throw Error("child должен быть унаследован от BaseControl");
            }
            return this._element.contains(child.get_element());
        };
        BaseControl.prototype.appendChild = function (child) {
            if (!(child instanceof BaseControl)) {
                throw Error("child должен быть унаследован от BaseControl");
            }
            this._element.appendChild(child.get_element());
        };
        BaseControl.prototype.appendChildAt = function (child, index) {
            if (!(child instanceof BaseControl)) {
                throw Error("child должен быть унаследован от BaseControl");
            }
            if (!index)
                index = 0;
            if (index >= this._element.children.length)
                this._element.appendChild(child.get_element());
            else
                this._element.insertBefore(child.get_element(), this._element.children[index]);
        };
        BaseControl.prototype.insertAt = function (child, index) {
            if (!(child instanceof BaseControl)) {
                throw Error("child должен быть унаследован от BaseControl");
            }
            if (!index)
                index = 0;
            if (index >= this._element.children.length)
                index = this._element.children.length;
            this._element.insertBefore(child.get_element(), this._element.children[index]);
        };
        BaseControl.prototype.removeChild = function (child) {
            if (!(child instanceof BaseControl)) {
                throw Error("child должен быть унаследован от BaseControl");
            }
            this._element.removeChild(child.get_element());
        };
        BaseControl.prototype.addEventListener = function (type, listener, options) {
            addEventListener_2.addEventListener(this._element, type, listener, options);
        };
        BaseControl.prototype.removeEventListener = function (type, listener) {
            removeEventListener_2.removeEventListener(this._element, type, listener);
        };
        BaseControl.prototype.dispatchEventWith = function (type, bubbles, cancelable, data) {
            if (bubbles === void 0) { bubbles = false; }
            if (cancelable === void 0) { cancelable = true; }
            EventUtil_1.EventUtil.dispatchEventWith(this._element, type, bubbles, cancelable, data);
        };
        BaseControl.prototype.addClass = function (names) {
            this._element.classList.add(names);
        };
        BaseControl.prototype.removeClass = function (names) {
            this._element.classList.remove(names);
        };
        BaseControl.prototype.removeAllListeners = function () {
            EventUtil_1.EventUtil.removeAllListeners(this._element);
        };
        BaseControl.prototype.dispose = function () {
            this.removeAllListeners();
            this._element = null;
        };
        return BaseControl;
    }());
    exports.BaseControl = BaseControl;
});
define("controls/Button", ["require", "exports", "controls/BaseControl"], function (require, exports, BaseControl_1) {
    "use strict";
    exports.__esModule = true;
    var Button = (function (_super) {
        __extends(Button, _super);
        function Button() {
            var _this = _super.call(this, 'div') || this;
            _this.addClass('button');
            _this.$down_handler = function (e) {
                _this.downState();
            };
            _this.addEventListener('mousedown', _this.$down_handler);
            _this.$up_handler = function (e) {
                _this.upState();
            };
            _this.addEventListener('mouseup', _this.$up_handler);
            return _this;
        }
        Button.prototype.get_name = function () { return this._name; };
        Button.prototype.set_name = function (v) { if (this._name != v)
            this._name = v; };
        Button.prototype.upState = function () { };
        Button.prototype.downState = function () { };
        Button.prototype.dispose = function () {
            this.removeEventListener('mousedown', this.$down_handler);
            this.$down_handler = null;
            this.removeEventListener('mouseup', this.$up_handler);
            this.$up_handler = null;
            _super.prototype.dispose.call(this);
        };
        return Button;
    }(BaseControl_1.BaseControl));
    exports.Button = Button;
});
define("controls/AdvancedButton", ["require", "exports", "controls/BaseControl", "controls/Button"], function (require, exports, BaseControl_2, Button_1) {
    "use strict";
    exports.__esModule = true;
    var AdvancedButton = (function (_super) {
        __extends(AdvancedButton, _super);
        function AdvancedButton() {
            var _this = _super.call(this) || this;
            _this._icon = new BaseControl_2.BaseControl();
            _this._icon.addClass('icon');
            _this.appendChild(_this._icon);
            _this._label = new BaseControl_2.BaseControl();
            _this._label.addClass('label');
            _this.appendChild(_this._label);
            return _this;
        }
        AdvancedButton.prototype.get_label = function () { return this._labelStr; };
        AdvancedButton.prototype.set_label = function (v) {
            if (this._labelStr != v) {
                this._labelStr = v;
                this._label.set_text(v);
            }
        };
        AdvancedButton.prototype.get_iconRenderer = function () { return this._icon; };
        AdvancedButton.prototype.get_labelRenderer = function () { return this._label; };
        AdvancedButton.prototype.upState = function () {
            _super.prototype.upState.call(this);
            this._label.removeClass('pressed');
        };
        AdvancedButton.prototype.downState = function () {
            _super.prototype.downState.call(this);
            this._label.addClass('pressed');
        };
        AdvancedButton.prototype.dispose = function () {
            this.removeChild(this._label);
            this._label.dispose();
            this._label = null;
            this.removeChild(this._icon);
            this._icon.dispose();
            this._icon = null;
            _super.prototype.dispose.call(this);
        };
        return AdvancedButton;
    }(Button_1.Button));
    exports.AdvancedButton = AdvancedButton;
});
define("models/IProductModel", ["require", "exports"], function (require, exports) {
    "use strict";
    exports.__esModule = true;
});
define("models/IOrderModel", ["require", "exports"], function (require, exports) {
    "use strict";
    exports.__esModule = true;
});
define("models/IStateStyleModel", ["require", "exports"], function (require, exports) {
    "use strict";
    exports.__esModule = true;
});
define("models/IStateModel", ["require", "exports"], function (require, exports) {
    "use strict";
    exports.__esModule = true;
});
define("models/IOrderStageLayoutColumnModel", ["require", "exports"], function (require, exports) {
    "use strict";
    exports.__esModule = true;
});
define("models/IOrderStageLayoutModel", ["require", "exports"], function (require, exports) {
    "use strict";
    exports.__esModule = true;
});
define("models/IOrderStageModel", ["require", "exports"], function (require, exports) {
    "use strict";
    exports.__esModule = true;
});
define("models/ISettingsModel", ["require", "exports"], function (require, exports) {
    "use strict";
    exports.__esModule = true;
});
define("models", ["require", "exports"], function (require, exports) {
    "use strict";
    exports.__esModule = true;
});
define("controls/ProductItemRenderer", ["require", "exports", "controls/BaseControl"], function (require, exports, BaseControl_3) {
    "use strict";
    exports.__esModule = true;
    var ProductItemRendererEventTypes;
    (function (ProductItemRendererEventTypes) {
        ProductItemRendererEventTypes["SELECT"] = "product-select";
        ProductItemRendererEventTypes["CHANGE_STATE_FOR_LAST_PRODUCT"] = "change-last-product-state";
    })(ProductItemRendererEventTypes = exports.ProductItemRendererEventTypes || (exports.ProductItemRendererEventTypes = {}));
    var ProductItemRenderer = (function (_super) {
        __extends(ProductItemRenderer, _super);
        function ProductItemRenderer(_data, _statesMap) {
            var _this = _super.call(this) || this;
            _this._data = _data;
            _this._statesMap = _statesMap;
            _this._isFirst = false;
            _this._isInner = false;
            _this._isLast = false;
            _this.isPiece = false;
            _this.addClass('product');
            _this.set_text(_this._data.name);
            _this.$select_handler = function (e) {
                e.stopImmediatePropagation();
                e.preventDefault();
                _this.dispatchEventWith(ProductItemRendererEventTypes.SELECT, true, false, _this);
            };
            _this.addEventListener('click', _this.$select_handler);
            _this.applyStyle();
            return _this;
        }
        ProductItemRenderer.prototype.get_data = function () { return this._data; };
        ProductItemRenderer.prototype.set_data = function (v) {
            if (this._data != v) {
                this._data = v;
                this.set_text(this._data.name);
                this.applyStyle();
            }
        };
        ProductItemRenderer.prototype.get_isFirst = function () { return this._isFirst; };
        ProductItemRenderer.prototype.set_isFirst = function (v) {
            if (this._isFirst != v) {
                this._isFirst = v;
                if (v)
                    this.addClass('first');
                else
                    this.removeClass('first');
            }
        };
        ProductItemRenderer.prototype.get_isInner = function () { return this._isInner; };
        ProductItemRenderer.prototype.set_isInner = function (v) {
            if (this._isInner != v) {
                this._isInner = v;
                if (v)
                    this.addClass('inner');
                else
                    this.removeClass('inner');
            }
        };
        ProductItemRenderer.prototype.get_isLast = function () { return this._isLast; };
        ProductItemRenderer.prototype.set_isLast = function (v) {
            if (this._isLast != v) {
                this._isLast = v;
                if (v)
                    this.addClass('last');
                else
                    this.removeClass('last');
            }
        };
        ProductItemRenderer.prototype.getState = function () {
            return this._statesMap ? this._statesMap[this._data.state] : undefined;
        };
        ProductItemRenderer.prototype.getStyle = function () {
            var state = this.getState();
            return state ? state.style : null;
        };
        ProductItemRenderer.prototype.applyStyle = function () {
            this.removeClass('state-' + String(this._lastState));
            var state = this.getState();
            if (state) {
                this._lastState = state.id;
                this.addClass('state-' + String(this._lastState));
            }
            var style = this.getStyle();
            if (style) {
                this.setStyle(style);
            }
            if (this._isLast) {
                this.dispatchEventWith(ProductItemRendererEventTypes.CHANGE_STATE_FOR_LAST_PRODUCT, true, false, this);
            }
        };
        ProductItemRenderer.prototype.dispose = function () {
            this.removeEventListener('click', this.$select_handler);
            this.$select_handler = null;
            _super.prototype.dispose.call(this);
        };
        return ProductItemRenderer;
    }(BaseControl_3.BaseControl));
    exports.ProductItemRenderer = ProductItemRenderer;
});
define("controls/OrderItemRenderer", ["require", "exports", "controls/BaseControl", "controls/ProductItemRenderer"], function (require, exports, BaseControl_4, ProductItemRenderer_1) {
    "use strict";
    exports.__esModule = true;
    var OrderPieceData = (function () {
        function OrderPieceData(startIndex, endIndex) {
            this.startIndex = startIndex;
            this.endIndex = endIndex;
        }
        return OrderPieceData;
    }());
    exports.OrderPieceData = OrderPieceData;
    var OrderPieceMetricData = (function (_super) {
        __extends(OrderPieceMetricData, _super);
        function OrderPieceMetricData(startIndex, endIndex, length) {
            if (startIndex === void 0) { startIndex = 0; }
            if (endIndex === void 0) { endIndex = 0; }
            if (length === void 0) { length = 1; }
            var _this = _super.call(this, startIndex, endIndex) || this;
            _this.length = length;
            return _this;
        }
        OrderPieceMetricData.prototype.setCenter = function () {
            this.startIndex = this.endIndex = 1;
            this.length = 3;
        };
        OrderPieceMetricData.prototype.setFirst = function () {
            this.startIndex = this.endIndex = 0;
            this.length = 2;
        };
        OrderPieceMetricData.prototype.setLast = function () {
            this.startIndex = this.endIndex = 1;
            this.length = 2;
        };
        OrderPieceMetricData.prototype.setAll = function () {
            this.startIndex = this.endIndex = 0;
            this.length = 1;
        };
        return OrderPieceMetricData;
    }(OrderPieceData));
    exports.OrderPieceMetricData = OrderPieceMetricData;
    var OrderItemRendererEventTypes;
    (function (OrderItemRendererEventTypes) {
        OrderItemRendererEventTypes["SELECT"] = "order-select";
    })(OrderItemRendererEventTypes = exports.OrderItemRendererEventTypes || (exports.OrderItemRendererEventTypes = {}));
    var OrderItemRendererType;
    (function (OrderItemRendererType) {
        OrderItemRendererType[OrderItemRendererType["FIRST"] = 0] = "FIRST";
        OrderItemRendererType[OrderItemRendererType["CENTER"] = 1] = "CENTER";
        OrderItemRendererType[OrderItemRendererType["LAST"] = 2] = "LAST";
        OrderItemRendererType[OrderItemRendererType["ALL"] = 3] = "ALL";
    })(OrderItemRendererType = exports.OrderItemRendererType || (exports.OrderItemRendererType = {}));
    var OrderItemRenderer = (function (_super) {
        __extends(OrderItemRenderer, _super);
        function OrderItemRenderer(_data, _statesMap, _pieceData, _metricData) {
            var _this = _super.call(this) || this;
            _this._data = _data;
            _this._statesMap = _statesMap;
            _this._pieceData = _pieceData;
            _this._metricData = _metricData;
            _this._isHidden = false;
            _this._renderersList = [];
            _this._componentMap = {};
            _this._collectionIndexMap = {};
            _this._rendererIndexMap = {};
            _this.addClass('order-renderer');
            var pieceData = _this._metricData ? _metricData : _this._pieceData;
            _this._isFirstPiece = !pieceData || (pieceData && pieceData.startIndex == 0);
            _this._isLastPiece = !pieceData || (_metricData ? (_metricData.endIndex == _metricData.length - 1) : (pieceData && pieceData.endIndex == _this._data.orderlines.length - 1));
            if (_this._isFirstPiece && _this._isLastPiece)
                _this._type = OrderItemRendererType.ALL;
            else if (_this._isFirstPiece)
                _this._type = OrderItemRendererType.FIRST;
            else if (_this._isLastPiece)
                _this._type = OrderItemRendererType.LAST;
            else
                _this._type = OrderItemRendererType.CENTER;
            if (_this._type == OrderItemRendererType.FIRST || _this._type == OrderItemRendererType.CENTER) {
                _this._bottomCropper = new BaseControl_4.BaseControl();
                _this._bottomCropper.addClass('bottom-cropper');
                _this.addClass('not-last-piece');
                _this.appendChild(_this._bottomCropper);
            }
            if (_this._type == OrderItemRendererType.CENTER || _this._type == OrderItemRendererType.LAST) {
                if (_this._type == OrderItemRendererType.CENTER)
                    _this.addClass('center-piece');
                _this.addClass('second-piece');
                _this._topCropper = new BaseControl_4.BaseControl();
                _this._topCropper.addClass('top-cropper');
                _this.appendChild(_this._topCropper);
            }
            else if (_this._type == OrderItemRendererType.FIRST)
                _this.addClass('first-piece');
            if (_this._metricData) {
            }
            else {
                _this._header = new BaseControl_4.BaseControl();
                _this._header.addClass('header');
                _this._orderNum = new BaseControl_4.BaseControl();
                _this._orderNum.addClass('order-num');
                _this._orderNum.set_text(String(_this._data.orderNum));
                _this._header.appendChild(_this._orderNum);
                _this._times = new BaseControl_4.BaseControl();
                _this._times.addClass('times');
                _this._header.appendChild(_this._times);
                _this._readyTime = new BaseControl_4.BaseControl();
                _this._readyTime.addClass('ready');
                _this._times.appendChild(_this._readyTime);
                _this._startTime = new BaseControl_4.BaseControl();
                _this._startTime.addClass('start');
                _this._times.appendChild(_this._startTime);
                _this._tableName = new BaseControl_4.BaseControl();
                _this._tableName.addClass('table-name');
                _this._tableName.set_text(_this._data.tableName);
                _this._header.appendChild(_this._tableName);
                _this.appendChild(_this._header);
                _this._products = new BaseControl_4.BaseControl();
                _this._products.addClass('products');
                _this.appendChild(_this._products);
                if (_this._isLastPiece) {
                    _this._footer = new BaseControl_4.BaseControl();
                    _this._footer.addClass('footer');
                    _this._leftTimeContainer = new BaseControl_4.BaseControl();
                    _this._leftTimeContainer.addClass('left-time');
                    _this._footer.appendChild(_this._leftTimeContainer);
                    _this._leftTimeM = new BaseControl_4.BaseControl();
                    _this._leftTimeM.addClass('minutes');
                    _this._leftTimeContainer.appendChild(_this._leftTimeM);
                    _this._leftTimeS = new BaseControl_4.BaseControl();
                    _this._leftTimeS.addClass('seconds');
                    _this._leftTimeContainer.appendChild(_this._leftTimeS);
                    _this._waiterName = new BaseControl_4.BaseControl();
                    _this._waiterName.set_text(_this._data.waiterName);
                    _this._waiterName.addClass('waiter-name');
                    _this._footer.appendChild(_this._waiterName);
                    _this.appendChild(_this._footer);
                }
                _this.$select_handler = function (e) {
                    e.stopImmediatePropagation();
                    _this.dispatchEventWith(OrderItemRendererEventTypes.SELECT, true, false, _this);
                };
                _this.addEventListener('click', _this.$select_handler);
                _this.$selectProduct_handler = function (e) {
                    e.stopImmediatePropagation();
                    var product = e.data;
                    var productModel = product.get_data();
                    var n = Number(productModel.state) + 1;
                    if (n > 4)
                        n = 1;
                    productModel.state = n;
                    product.applyStyle();
                    console.log('select dish: ' + productModel.guid);
                };
                _this.addEventListener(ProductItemRenderer_1.ProductItemRendererEventTypes.SELECT, _this.$selectProduct_handler);
                _this.$changeLastProductState_handler = function (e) {
                    e.stopImmediatePropagation();
                    var product = e.data;
                    var productModel = product.get_data();
                    _this.changeStateForBottomCropper(productModel.state);
                };
                _this.addEventListener(ProductItemRenderer_1.ProductItemRendererEventTypes.CHANGE_STATE_FOR_LAST_PRODUCT, _this.$changeLastProductState_handler);
                if (_this._data && _this._data.orderlines)
                    _this.displayItems();
                else
                    _this.removeItems();
                _this.applyStyle();
                _this.calculateStaticTimes();
                _this.tick(new Date());
            }
            return _this;
        }
        OrderItemRenderer.prototype.get_isHidden = function () { return this._isHidden; };
        OrderItemRenderer.prototype.set_isHidden = function (v) { if (this._isHidden != v)
            this._isHidden = v; };
        OrderItemRenderer.prototype.get_pieceData = function () { return this._pieceData; };
        OrderItemRenderer.prototype.get_isPiece = function () { return Boolean(this._pieceData); };
        OrderItemRenderer.prototype.get_type = function () { return this._type; };
        OrderItemRenderer.prototype.get_isFirstPiece = function () { return this._isFirstPiece; };
        OrderItemRenderer.prototype.get_isLastPiece = function () { return this._isLastPiece; };
        OrderItemRenderer.prototype.get_data = function () { return this._data; };
        OrderItemRenderer.prototype.set_data = function (v) {
            if (this._data != v) {
                ;
                this._data = v;
                if (this._data && this._data.orderlines)
                    this.displayItems();
                else
                    this.removeItems();
                this.applyStyle();
            }
        };
        OrderItemRenderer.prototype.changeStateForBottomCropper = function (state) {
            if (this._bottomCropper) {
                if (this._lastRendererState)
                    this._bottomCropper.removeClass('state-' + String(this._lastRendererState));
                this._lastRendererState = state;
                this._bottomCropper.addClass('state-' + String(this._lastRendererState));
            }
        };
        OrderItemRenderer.prototype.setTestState = function (state) {
            this.applyStyle(state);
            this.changeStateForBottomCropper(state.id);
        };
        OrderItemRenderer.prototype.get_AdditionalHeight = function () {
            return { a: (this._topCropper ? this._topCropper.get_height() : 0), b: (this._bottomCropper ? this._bottomCropper.get_height() : 0) };
        };
        OrderItemRenderer.prototype.displayItems = function () {
            var collection = this._data.orderlines;
            if (this._renderersList.length > 0) {
                if (this._renderersList.length > 1) {
                    this._renderersList[0].set_isFirst(false);
                    this._renderersList[0].set_isInner(false);
                }
                var lastIndex = this._renderersList.length - 1;
                this._renderersList[lastIndex].set_isLast(false);
                this._renderersList[lastIndex].set_isInner(false);
                for (var i = this._renderersList.length - 1; i >= 0; i--) {
                    var renderer = this._renderersList[i];
                    if ((this._pieceData && (i < this._rendererIndexMap[this._pieceData.startIndex] || i > this._rendererIndexMap[this._pieceData.endIndex])) || !this.existsProduct(collection, renderer.get_data().guid)) {
                        this._componentMap[renderer.get_data().guid] = null;
                        this._renderersList.splice(i, 1);
                        this._products.removeChild(renderer);
                        delete this._componentMap[renderer.get_data().guid];
                        var collectionIndex = this._rendererIndexMap[this._renderersList.length];
                        delete this._rendererIndexMap[this._renderersList.length];
                        delete this._collectionIndexMap[collectionIndex];
                        renderer.dispose();
                        renderer = null;
                    }
                }
            }
            var i = 0;
            for (var _i = 0, collection_1 = collection; _i < collection_1.length; _i++) {
                var itemData = collection_1[_i];
                if (!this._pieceData || (i >= this._pieceData.startIndex && i <= this._pieceData.endIndex)) {
                    this._rendererIndexMap[this._renderersList.length] = i;
                    this._collectionIndexMap[i] = this._renderersList.length;
                    if (this._componentMap.hasOwnProperty(itemData.guid)) {
                        var renderer = this._componentMap[itemData.guid];
                        renderer.set_data(itemData);
                    }
                    else {
                        var renderer = new ProductItemRenderer_1.ProductItemRenderer(itemData, this._statesMap);
                        renderer.set_isInner(true);
                        this._renderersList.push(renderer);
                        this._componentMap[itemData.guid] = renderer;
                        this._products.appendChild(renderer);
                    }
                }
                i++;
            }
            if (this._renderersList.length > 0) {
                if (this._renderersList.length > 1) {
                    this._renderersList[0].set_isFirst(true);
                    this._renderersList[0].set_isInner(false);
                }
                var lastIndex = this._renderersList.length - 1;
                this._renderersList[lastIndex].set_isLast(true);
                this._renderersList[lastIndex].set_isInner(false);
            }
            collection = null;
            if (this._bottomCropper) {
                this.insertAt(this._bottomCropper, this.get_childrenLength());
            }
        };
        OrderItemRenderer.prototype.getOrderLinesAsString = function () {
            var result = '';
            for (var _i = 0, _a = this._renderersList; _i < _a.length; _i++) {
                var product = _a[_i];
                result += ', ' + product.get_data().name;
            }
            return result;
        };
        OrderItemRenderer.prototype.existsProduct = function (collection, guid) {
            for (var _i = 0, collection_2 = collection; _i < collection_2.length; _i++) {
                var productData = collection_2[_i];
                if (productData.guid == guid)
                    return true;
            }
            return false;
        };
        OrderItemRenderer.prototype.removeItems = function () {
            for (var _i = 0, _a = this._renderersList; _i < _a.length; _i++) {
                var renderer = _a[_i];
                this._products.removeChild(renderer);
                renderer.dispose();
                renderer = null;
            }
            for (var k in this._componentMap) {
                this._componentMap[k] = null;
                delete this._componentMap[k];
            }
            this._renderersList.length = 0;
        };
        OrderItemRenderer.prototype.tick = function (dt) {
            this.calcPassedTime(dt);
            if (this._leftTimeM)
                this._leftTimeM.set_text(String(this._tpassedM));
            if (this._leftTimeS)
                this._leftTimeS.set_text(String(this._tpassedS));
        };
        OrderItemRenderer.prototype.calculateStaticTimes = function () {
            this._dtstart = new Date();
            this._dtstart.setTime(this._data.startedLong);
            this._tstart = this._dtstart.getTime();
            this._dtready = new Date();
            this._dtready.setTime(this._data.normTimeLong);
            this._tready = this._dtready.getTime();
            this._tready = moment(this._dtstart).format("mm:ss");
            this._tstart = moment(this._dtready).format('mm:ss');
            if (this._header) {
                this._startTime.set_text(String(this._tstart));
                this._readyTime.set_text(String(this._tready));
            }
        };
        OrderItemRenderer.prototype.calcPassedTime = function (dt) {
            this._tpassedM = moment(dt).format("mm");
            this._tpassedS = moment(dt).format("ss");
        };
        OrderItemRenderer.prototype.getState = function () {
            return this._statesMap ? this._statesMap[this._data.state] : undefined;
        };
        OrderItemRenderer.prototype.getStyle = function (stateModel) {
            var state = stateModel ? stateModel : this.getState();
            return state ? state.style : null;
        };
        OrderItemRenderer.prototype.applyStyle = function (customState) {
            this.removeClass('state-' + String(this._lastState));
            var state = customState ? customState : this.getState();
            if (state) {
                this._lastState = state.id;
                this.addClass('state-' + String(this._lastState));
            }
            for (var i = 0, l = this._renderersList.length; i < l; i++) {
                var renderer = this._renderersList[i];
                renderer.applyStyle();
            }
        };
        OrderItemRenderer.prototype.divideByHeight = function (waterline, maxHeight, metric) {
            if (this._renderersList.length > 1) {
                var h = this.get_height();
                if (this._footer)
                    h -= Number(this._footer.get_height());
                for (var i = this._renderersList.length - 1; i >= 1; i--) {
                    var product_1 = this._renderersList[i];
                    var m_1 = this.calcAdditionalParams(metric, product_1.get_data().state, 0, i - 1, i, this._renderersList.length - 1);
                    h -= Number(product_1.get_height());
                    if (Number(h + m_1.ah) <= waterline) {
                        var one = new OrderItemRenderer(this._data, this._statesMap, new OrderPieceData(m_1.s1, m_1.s2));
                        var two = new OrderItemRenderer(this._data, this._statesMap, new OrderPieceData(m_1.e1, m_1.e2));
                        return {
                            one: one,
                            two: two,
                            isDivided: true,
                            isMandatory: false
                        };
                    }
                }
                h = this._header ? this._header.get_height() : 0;
                var product = this._renderersList[0];
                var m = this.calcAdditionalParams(metric, product.get_data().state, 0, 0, 1, this._renderersList.length - 1);
                h += Number(this._renderersList[0].get_height() - m.ah);
                if (h >= maxHeight) {
                    var one = new OrderItemRenderer(this._data, this._statesMap, new OrderPieceData(m.s1, m.s2));
                    var two = new OrderItemRenderer(this._data, this._statesMap, new OrderPieceData(m.e1, m.e2));
                    return {
                        one: one,
                        two: two,
                        isDivided: true,
                        isMandatory: true
                    };
                }
            }
            return null;
        };
        OrderItemRenderer.prototype.calcAdditionalParams = function (metric, state, a1, a2, b1, b2) {
            var s1 = this._rendererIndexMap[a1];
            var s2 = this._rendererIndexMap[a2];
            var e1 = this._rendererIndexMap[b1];
            var e2 = this._rendererIndexMap[b2];
            var f = s1 == 0;
            var l = s2 == this._data.orderlines.length - 1;
            var m = metric[state];
            var ah = 0;
            if (!f)
                ah += Number(m.a);
            if (!l)
                ah += Number(m.b);
            return { s1: s1, s2: s2, e1: e1, e2: e2, ah: ah };
        };
        OrderItemRenderer.prototype.dispose = function () {
            this._dtstart = null;
            this._dtready = null;
            this.removeEventListener('click', this.$select_handler);
            this.$select_handler = null;
            this.removeEventListener(ProductItemRenderer_1.ProductItemRendererEventTypes.SELECT, this.$selectProduct_handler);
            this.$selectProduct_handler = null;
            this.removeEventListener(ProductItemRenderer_1.ProductItemRendererEventTypes.CHANGE_STATE_FOR_LAST_PRODUCT, this.$changeLastProductState_handler);
            this.$changeLastProductState_handler = null;
            this.removeItems();
            this._renderersList = null;
            this._componentMap = null;
            if (this._topCropper) {
                this.removeChild(this._topCropper);
                this._topCropper.dispose();
                this._topCropper = null;
            }
            if (this._bottomCropper) {
                if (this.contains(this._bottomCropper))
                    this.removeChild(this._bottomCropper);
                this._bottomCropper.dispose();
                this._bottomCropper = null;
            }
            if (this._header) {
                this._header.removeChild(this._orderNum);
                this._orderNum.dispose();
                this._orderNum = null;
                this._times.removeChild(this._startTime);
                this._startTime.dispose();
                this._startTime = null;
                this._times.removeChild(this._readyTime);
                this._readyTime.dispose();
                this._readyTime = null;
                this._header.removeChild(this._times);
                this._times.dispose();
                this._times = null;
                this._header.removeChild(this._tableName);
                this._tableName.dispose();
                this._tableName = null;
                this.removeChild(this._header);
                this._header.dispose();
                this._header = null;
            }
            if (this._footer) {
                this._leftTimeContainer.removeChild(this._leftTimeM);
                this._leftTimeM.dispose();
                this._leftTimeM = null;
                this._leftTimeContainer.removeChild(this._leftTimeS);
                this._leftTimeS.dispose();
                this._leftTimeS = null;
                this._footer.removeChild(this._leftTimeContainer);
                this._leftTimeContainer.dispose();
                this._leftTimeContainer = null;
                this._footer.removeChild(this._waiterName);
                this._waiterName.dispose();
                this._waiterName = null;
                this.removeChild(this._footer);
                this._footer.dispose();
                this._footer = null;
            }
            this._data = null;
            this._statesMap = null;
            this._pieceData = null;
            this._tpassedM = null;
            this._tpassedS = null;
            this._tstart = null;
            this._tready = null;
            _super.prototype.dispose.call(this);
        };
        return OrderItemRenderer;
    }(BaseControl_4.BaseControl));
    exports.OrderItemRenderer = OrderItemRenderer;
});
define("events/EventDispatcher", ["require", "exports", "events/Event"], function (require, exports, Event_1) {
    "use strict";
    exports.__esModule = true;
    var EventDispatcher = (function () {
        function EventDispatcher() {
            this._eventStack = [];
        }
        EventDispatcher.prototype.addEventListener = function (type, listener) {
            if (this._eventListeners == null)
                this._eventListeners = {};
            var listeners = this._eventListeners[type];
            if (listeners == null)
                this._eventListeners[type] = [listener];
            else if (listeners.indexOf(listener) == -1)
                listeners[listeners.length] = listener;
        };
        EventDispatcher.prototype.removeEventListener = function (type, listener) {
            if (this._eventListeners) {
                var listeners = this._eventListeners[type];
                var numListeners = listeners ? listeners.length : 0;
                if (numListeners > 0) {
                    var index = listeners.indexOf(listener);
                    if (index != -1) {
                        if (this._eventStack.indexOf(type) == -1) {
                            listeners.splice(index, 1);
                        }
                        else {
                            var restListeners = listeners.slice(0, index);
                            for (var i = index + 1; i < numListeners; ++i) {
                                restListeners[i - 1] = listeners[i];
                            }
                            this._eventListeners[type] = restListeners;
                        }
                    }
                }
            }
        };
        EventDispatcher.prototype.removeEventListeners = function (type) {
            if (type && this._eventListeners)
                delete this._eventListeners[type];
            else
                this._eventListeners = null;
        };
        EventDispatcher.prototype.dispatchEvent = function (event) {
            var bubbles = event.get_bubbles();
            if (!bubbles && (this._eventListeners == null || !this._eventListeners.hasOwnProperty(event.get_type())))
                return;
            var previousTarget = event.get_target();
            event.setTarget(this);
            this.invokeEvent(event);
            if (previousTarget)
                event.setTarget(previousTarget);
        };
        EventDispatcher.prototype.invokeEvent = function (event) {
            var listeners = this._eventListeners ? this._eventListeners[event.get_type()] : null;
            var numListeners = listeners == null ? 0 : listeners.length;
            if (numListeners) {
                event.setCurrentTarget(this);
                this._eventStack[this._eventStack.length] = event.get_type();
                for (var i = 0; i < numListeners; ++i) {
                    var listener = listeners[i];
                    var numArgs = listener.length;
                    if (numArgs == 0)
                        listener();
                    else if (numArgs == 1)
                        listener(event);
                    else
                        listener(event, event.get_data());
                    if (event.get_stopsImmediatePropagation())
                        return true;
                }
                this._eventStack.pop();
                return event.get_stopsPropagation();
            }
            else {
                return false;
            }
        };
        EventDispatcher.prototype.dispatchEventWith = function (type, bubbles, data) {
            if (bubbles === void 0) { bubbles = false; }
            if (data === void 0) { data = null; }
            if (bubbles || this.hasEventListener(type)) {
                var event = Event_1.Event.fromPool(type, bubbles, data);
                this.dispatchEvent(event);
                Event_1.Event.toPool(event);
            }
        };
        EventDispatcher.prototype.hasEventListener = function (type, listener) {
            var listeners = this._eventListeners ? this._eventListeners[type] : null;
            if (listeners == null)
                return false;
            else {
                if (listener != null)
                    return listeners.indexOf(listener) != -1;
                else
                    return listeners.length != 0;
            }
        };
        EventDispatcher.prototype.dispose = function () {
            this.removeEventListeners();
        };
        return EventDispatcher;
    }());
    exports.EventDispatcher = EventDispatcher;
});
define("events/Event", ["require", "exports"], function (require, exports) {
    "use strict";
    exports.__esModule = true;
    var Event = (function () {
        function Event(_type, _bubbles, _data) {
            if (_bubbles === void 0) { _bubbles = false; }
            this._type = _type;
            this._bubbles = _bubbles;
            this._data = _data;
        }
        Event.prototype.stopPropagation = function () {
            this._stopsPropagation = true;
        };
        Event.prototype.stopImmediatePropagation = function () {
            this._stopsPropagation = this._stopsImmediatePropagation = true;
        };
        Event.prototype.get_bubbles = function () { return this._bubbles; };
        Event.prototype.get_target = function () {
            return this._target;
        };
        Event.prototype.get_currentTarget = function () {
            return this._currentTarget;
        };
        Event.prototype.get_type = function () {
            return this._type;
        };
        Event.prototype.get_data = function () {
            return this._data;
        };
        Event.prototype.setTarget = function (value) {
            this._target = value;
        };
        Event.prototype.setCurrentTarget = function (value) {
            this._currentTarget = value;
        };
        Event.prototype.setData = function (value) {
            this._data = value;
        };
        Event.prototype.get_stopsPropagation = function () {
            return this._stopsPropagation;
        };
        Event.prototype.get_stopsImmediatePropagation = function () {
            return this._stopsImmediatePropagation;
        };
        Event.fromPool = function (type, bubbles, data) {
            if (bubbles === void 0) { bubbles = false; }
            if (Event.eventPool.length > 0)
                return Event.eventPool.pop().reset(type, bubbles, data);
            else
                return new Event(type, bubbles, data);
        };
        Event.toPool = function (event) {
            event._data = event._target = event._currentTarget = null;
            Event.eventPool[Event.eventPool.length] = event;
        };
        Event.prototype.reset = function (type, bubbles, data) {
            if (bubbles === void 0) { bubbles = false; }
            if (data === void 0) { data = null; }
            this._type = type;
            this._bubbles = bubbles;
            this._data = data;
            this._target = this._currentTarget = null;
            this._stopsPropagation = this._stopsImmediatePropagation = false;
            return this;
        };
        Event.FAIL = 'fail';
        Event.PROGRESS = 'progress';
        Event.eventPool = [];
        return Event;
    }());
    exports.Event = Event;
});
define("events", ["require", "exports", "events/Event", "events/EventDispatcher", "events/EventUtil"], function (require, exports, Event_2, EventDispatcher_1, EventUtil_2) {
    "use strict";
    function __export(m) {
        for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
    }
    exports.__esModule = true;
    __export(Event_2);
    __export(EventDispatcher_1);
    __export(EventUtil_2);
});
define("data/DataLoader", ["require", "exports", "events"], function (require, exports, events_1) {
    "use strict";
    exports.__esModule = true;
    var DataLoaderContentType;
    (function (DataLoaderContentType) {
        DataLoaderContentType[DataLoaderContentType["JSON"] = 0] = "JSON";
        DataLoaderContentType[DataLoaderContentType["TEXT"] = 1] = "TEXT";
    })(DataLoaderContentType = exports.DataLoaderContentType || (exports.DataLoaderContentType = {}));
    var DataLoaderEventTypes;
    (function (DataLoaderEventTypes) {
        DataLoaderEventTypes["DATA"] = "data";
        DataLoaderEventTypes["ERROR"] = "error";
    })(DataLoaderEventTypes = exports.DataLoaderEventTypes || (exports.DataLoaderEventTypes = {}));
    var DataLoader = (function (_super) {
        __extends(DataLoader, _super);
        function DataLoader(_token) {
            var _this = _super.call(this) || this;
            _this._token = _token;
            _this._isLoading = false;
            _this._isComplete = false;
            _this._autoLoad = false;
            return _this;
        }
        DataLoader.prototype.get_content = function () { return this._content; };
        DataLoader.prototype.get_autoLoad = function () { return this._autoLoad; };
        DataLoader.prototype.set_autoLoad = function (v) {
            if (this._autoLoad != v) {
                if (!this._isLoading && !this._isComplete && this._source)
                    this.load();
            }
        };
        DataLoader.prototype.get_source = function () { return this._source; };
        DataLoader.prototype.set_source = function (v) {
            if (this._source != v) {
                this._source = v;
                if (this._source && this._autoLoad)
                    this.load();
            }
        };
        DataLoader.prototype.get_contentType = function () { return this._contentType; };
        DataLoader.prototype.set_contentType = function (v) {
            if (this._contentType != v) {
                this._contentType = v;
                this.transformContentIfNeed();
            }
        };
        DataLoader.prototype.set_params = function (params) {
            this._params = params;
        };
        DataLoader.prototype.load = function () {
            var _this = this;
            this._isLoading = true;
            this._isComplete = false;
            if (this._request)
                this.unload();
            this._request = new XMLHttpRequest();
            this._request.onreadystatechange = function () {
                if (_this._request.readyState == 4 && _this._request.status == 200) {
                    _this._response = _this._request.responseText;
                    var error = false;
                    try {
                        _this.transformContentIfNeed();
                    }
                    catch (e) {
                        error = e;
                    }
                    if (error)
                        _this.dispatchEventWith.call(_this, DataLoaderEventTypes.ERROR, false, error);
                    _this.dispatchEventWith.call(_this, DataLoaderEventTypes.DATA, false, _this._content);
                    if (_this._request)
                        _this.unload();
                    _this._isComplete = true;
                }
            };
            var queryParams = this.getQueryParamsAsString();
            this._request.open("GET", this._source + queryParams, true);
            if (this._token)
                this._request.setRequestHeader('token', this._token);
            this._request.send();
        };
        DataLoader.prototype.getQueryParamsAsString = function () {
            var result = "";
            if (this._params) {
                for (var i = 0, l = this._params.length; i < l; i++) {
                    var param = this._params[i];
                    if (i == 0)
                        result += "?";
                    result += param.key + "=" + String(param.value);
                    if (i < l - 1)
                        result += "&";
                }
            }
            return result;
        };
        DataLoader.prototype.transformContentIfNeed = function () {
            switch (this._contentType) {
                case DataLoaderContentType.JSON:
                    if (this._response)
                        this._content = JSON.parse(this._response);
                    break;
                default:
                    this._content = this._response;
            }
        };
        DataLoader.prototype.unload = function () {
            this._isLoading = false;
            this._request.onreadystatechange = null;
            this._request.abort();
            this._request = null;
            this._content = null;
        };
        DataLoader.prototype.dispose = function () {
            this.removeEventListeners();
            this._response = null;
            if (this._request)
                this.unload();
            this._params = null;
            _super.prototype.dispose.call(this);
        };
        return DataLoader;
    }(events_1.EventDispatcher));
    exports.DataLoader = DataLoader;
});
define("data/DynamicCSS", ["require", "exports"], function (require, exports) {
    "use strict";
    exports.__esModule = true;
    var DynamicCSS = (function () {
        function DynamicCSS(filename, cb) {
            this._ref = document.createElement("link");
            this._ref.setAttribute("rel", "stylesheet");
            this._ref.setAttribute("type", "text/css");
            this._ref.setAttribute("href", filename);
            this._ref.onload = function () {
                cb();
            };
            document.getElementsByTagName('head')[0].appendChild(this._ref);
        }
        return DynamicCSS;
    }());
    exports.DynamicCSS = DynamicCSS;
});
define("data", ["require", "exports", "data/DataLoader", "data/DynamicCSS"], function (require, exports, DataLoader_1, DynamicCSS_1) {
    "use strict";
    function __export(m) {
        for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
    }
    exports.__esModule = true;
    __export(DataLoader_1);
    __export(DynamicCSS_1);
});
define("core/Settings", ["require", "exports", "data", "events"], function (require, exports, data_1, events_2) {
    "use strict";
    exports.__esModule = true;
    var SettingsEventTypes;
    (function (SettingsEventTypes) {
        SettingsEventTypes["COMPLETE"] = "settings-complete";
    })(SettingsEventTypes = exports.SettingsEventTypes || (exports.SettingsEventTypes = {}));
    var Settings = (function (_super) {
        __extends(Settings, _super);
        function Settings(_data) {
            var _this = _super.call(this) || this;
            _this._data = _data;
            Settings.instance = _this;
            _this.calcLayoutColumnParts();
            _this.createStatesMap();
            _this.loadStatesCSS();
            return _this;
        }
        Settings.prototype.get_data = function () { return this._data; };
        Settings.prototype.get_statesMap = function () { return this._statesMap; };
        Settings.prototype.calcLayoutColumnParts = function () {
            this._date = new Date();
            for (var _i = 0, _a = this._data.orderStage; _i < _a.length; _i++) {
                var screen_1 = _a[_i];
                var tagList = [];
                var undefColumns = [];
                var unusedColumns = [];
                var totalParts = screen_1.layout.totalParts;
                for (var _b = 0, _c = screen_1.layout.columns; _b < _c.length; _b++) {
                    var column = _c[_b];
                    if (column.tag != undefined && String(column.parts) == "") {
                        if (tagList.indexOf(column.tag) == -1)
                            tagList.push(column.tag);
                        undefColumns.push(column);
                    }
                    else if (column.tag == undefined || column.parts <= 0)
                        unusedColumns.push(column);
                }
                while (unusedColumns.length > 0) {
                    var column = unusedColumns.pop();
                    var index = screen_1.layout.columns.indexOf(column);
                    if (index >= 0)
                        screen_1.layout.columns.splice(index, 1);
                }
                var fixedParts = 0;
                for (var _d = 0, _e = screen_1.layout.columns; _d < _e.length; _d++) {
                    var column = _e[_d];
                    fixedParts += Number(column.parts);
                }
                var dynamicParts = undefColumns.length > 0 ? (totalParts - fixedParts) / undefColumns.length : 0;
                for (var i = 0, l = undefColumns.length; i < l; i++) {
                    var column = undefColumns[i];
                    column.parts = dynamicParts;
                }
                undefColumns.length = 0;
                undefColumns = null;
                unusedColumns.length = 0;
                unusedColumns = null;
                screen_1.tagList = tagList;
            }
        };
        Settings.prototype.createStatesMap = function () {
            this._statesMap = {};
            this._styles = [];
            for (var _i = 0, _a = this._data.states; _i < _a.length; _i++) {
                var state = _a[_i];
                this._statesMap[state.id] = state;
                if (state.style && state.style.file)
                    this._styles.push({ id: state.id, link: state.style.file });
            }
        };
        Settings.prototype.loadStatesCSS = function () {
            var _this = this;
            if (this._styles.length > 0) {
                var style = this._styles.pop();
                var css = new data_1.DynamicCSS(style.link, function () {
                    _this.loadStatesCSS();
                });
            }
            else {
                this.dispatchEventWith.call(this, SettingsEventTypes.COMPLETE, false, true);
            }
        };
        Settings.prototype.dispose = function () {
            this._date = null;
            this._statesMap = null;
            _super.prototype.dispose.call(this);
        };
        Settings.demo = true;
        return Settings;
    }(events_2.EventDispatcher));
    exports.Settings = Settings;
});
define("controls/Column", ["require", "exports", "controls/BaseControl", "controls/OrderItemRenderer"], function (require, exports, BaseControl_5, OrderItemRenderer_1) {
    "use strict";
    exports.__esModule = true;
    var Column = (function (_super) {
        __extends(Column, _super);
        function Column(_selfLayout, _commonLayout, orderCollection, _statesMap) {
            var _this = _super.call(this) || this;
            _this._selfLayout = _selfLayout;
            _this._commonLayout = _commonLayout;
            _this._statesMap = _statesMap;
            _this._renderersList = [];
            _this._componentMap = {};
            _this._horizontalPosition = 0;
            _this.addClass("column");
            _this._renderersList = [];
            _this._componentMap = {};
            _this._horizontalPosition = _this.calculateHorizontalPosition();
            _this.set_orderCollection(orderCollection);
            _this.setStyle(_this._selfLayout.style);
            return _this;
        }
        Column.prototype.get_orderCollection = function () { return this._orderCollection; };
        Column.prototype.set_orderCollection = function (v) {
            if (this._orderCollection != v) {
                this._orderCollection = v;
                if (v)
                    this.displayItems();
                else
                    this.removeItems();
            }
        };
        Column.prototype.get_collection = function () {
            if (this._orderCollection && this._orderCollection.hasOwnProperty(this._selfLayout.tag))
                return this._orderCollection[this._selfLayout.tag];
            return [];
        };
        Column.prototype.get_statesMap = function () { return this._statesMap; };
        Column.prototype.tick = function (dt) {
            for (var _i = 0, _a = this._renderersList; _i < _a.length; _i++) {
                var renderer = _a[_i];
                if (renderer.get_isHidden())
                    continue;
                renderer.tick(dt);
            }
        };
        Column.prototype.setExplicitSize = function (pw, ph) {
            this._partWidth = pw;
            this._partHeight = ph;
            var pos = this._horizontalPosition;
            this.set_x(pos * this._partWidth);
            this.set_width(this._selfLayout.parts * this._partWidth);
            this.deletePiece();
            this.updateLayout();
        };
        Column.prototype.deletePiece = function () {
            if (this._renderersList && this._renderersList.length > 0) {
                for (var i = this._renderersList.length - 1; i >= 0; i--) {
                    var component = this._renderersList[i];
                    if (component.get_isHidden()) {
                        component.set_isHidden(false);
                        if (!this.contains(component)) {
                            this.appendChildAt(component, i);
                        }
                    }
                    if (component.get_isPiece()) {
                        this._renderersList.splice(i, 1);
                        var index = this._componentMap[component.get_data().guid].indexOf(component);
                        if (index > -1)
                            this._componentMap[component.get_data().guid].splice(index, 1);
                        if (this._componentMap[component.get_data().guid].length == 0)
                            delete this._componentMap[component.get_data().guid];
                        if (this.contains(component))
                            this.removeChild(component);
                        component.dispose();
                        component = null;
                    }
                }
            }
        };
        Column.prototype.updateLayout = function () {
            if (this._renderersList && this._renderersList.length > 0) {
                var x = Column.paddingX, y = Column.paddingY, page = 1, height = 0;
                var w = ((this._partWidth * this._selfLayout.parts) - (Column.paddingX * 2) - Column.gapX * (this._selfLayout.parts - 1)) / this._selfLayout.parts;
                var lastUndivIndex = -1;
                var metric = this.getMetric(w);
                for (var i = 0, l = this._renderersList.length; i < l; i++) {
                    var component = this._renderersList[i];
                    if (component.get_isHidden())
                        continue;
                    component.set_width(w);
                    var waterline = Math.ceil((page * this._partHeight) - Column.paddingY);
                    var sinking = Math.floor(y + component.get_height());
                    var undiv = false;
                    if (sinking > waterline) {
                        var renderers = void 0;
                        renderers = component.divideByHeight(Math.ceil(waterline - y), this._partHeight - (Column.paddingY * 2), metric);
                        if (renderers && renderers.isDivided) {
                            component.set_isHidden(true);
                            if (this.contains(component))
                                this.removeChild(component);
                            this._renderersList.splice(i, 0, renderers.two);
                            this._renderersList.splice(i, 0, renderers.one);
                            this.appendChildAt(renderers.one, i);
                            this.appendChildAt(renderers.two, i + 1);
                            this._componentMap[renderers.one.get_data().guid].push(renderers.one);
                            this._componentMap[renderers.two.get_data().guid].push(renderers.two);
                            l += Number(2);
                            i--;
                            continue;
                        }
                        if (lastUndivIndex != i) {
                            undiv = true;
                            lastUndivIndex = i;
                        }
                    }
                    component.set_x(x);
                    component.set_y(y);
                    var my = Number(y) + component.get_height();
                    height = Math.max(my, height);
                    y = Number(my + Column.gapY);
                    if (y - Column.gapY > waterline) {
                        if (this._selfLayout.parts > 1) {
                            x += Number(w + Column.gapX);
                            if (x >= w * this._selfLayout.parts) {
                                x = Column.paddingX;
                                page++;
                            }
                        }
                        else
                            page++;
                        y = ((page - 1) * this._partHeight) + Column.paddingY;
                    }
                    if (undiv)
                        i--;
                }
                this.set_height(Math.ceil(height / this._partHeight) * this._partHeight);
            }
            else {
                this.set_height(this._partHeight);
            }
        };
        Column.prototype.getMetric = function (width) {
            var result = {};
            var metric = new OrderItemRenderer_1.OrderPieceMetricData();
            metric.setCenter();
            var testElement = new OrderItemRenderer_1.OrderItemRenderer(null, this._statesMap, null, metric);
            document.getElementsByTagName('body')[0].appendChild(testElement.get_element());
            testElement.set_width(width);
            var sMap = this._statesMap;
            for (var s in sMap) {
                testElement.setTestState(sMap[s]);
                result[s] = testElement.get_AdditionalHeight();
            }
            document.getElementsByTagName('body')[0].removeChild(testElement.get_element());
            testElement.dispose();
            testElement = null;
            metric = null;
            return result;
        };
        Column.prototype.calculateHorizontalPosition = function () {
            var result = 0;
            if (this._selfLayout && this._commonLayout) {
                for (var _i = 0, _a = this._commonLayout; _i < _a.length; _i++) {
                    var l = _a[_i];
                    if (l == this._selfLayout)
                        break;
                    result += Number(l.parts);
                }
            }
            return result;
        };
        Column.prototype.displayItems = function () {
            this.deletePiece();
            var collection = this.get_collection();
            if (this._renderersList.length > 0) {
                for (var i = this._renderersList.length - 1; i >= 0; i--) {
                    var renderer = this._renderersList[i];
                    if (!this.existsOrder(collection, renderer.get_data().guid)) {
                        this._componentMap[renderer.get_data().guid] = null;
                        delete this._componentMap[renderer.get_data().guid];
                        this._renderersList.splice(i, 1);
                        if (this.contains(renderer))
                            this.removeChild(renderer);
                        renderer.dispose();
                        renderer = null;
                    }
                }
            }
            for (var _i = 0, collection_3 = collection; _i < collection_3.length; _i++) {
                var itemData = collection_3[_i];
                if (this._componentMap.hasOwnProperty(itemData.guid)) {
                    for (var _a = 0, _b = this._componentMap[itemData.guid]; _a < _b.length; _a++) {
                        var renderer = _b[_a];
                        renderer.set_data(itemData);
                    }
                }
                else {
                    var renderer = new OrderItemRenderer_1.OrderItemRenderer(itemData, this._statesMap);
                    this._renderersList.push(renderer);
                    if (!this._componentMap[itemData.guid])
                        this._componentMap[itemData.guid] = [];
                    this._componentMap[itemData.guid].push(renderer);
                    this.appendChild(renderer);
                }
            }
            collection = null;
            this.updateLayout();
        };
        Column.prototype.existsOrder = function (collection, guid) {
            for (var _i = 0, collection_4 = collection; _i < collection_4.length; _i++) {
                var orderData = collection_4[_i];
                if (orderData.guid == guid)
                    return true;
            }
            return false;
        };
        Column.prototype.removeItems = function () {
            for (var _i = 0, _a = this._renderersList; _i < _a.length; _i++) {
                var renderer = _a[_i];
                if (this.contains(renderer))
                    this.removeChild(renderer);
                renderer.dispose();
                renderer = null;
            }
            for (var k in this._componentMap) {
                this._componentMap[k] = null;
                delete this._componentMap[k];
            }
            this._renderersList.length = 0;
        };
        Column.prototype.dispose = function () {
            this.removeItems();
            this._renderersList = null;
            this._componentMap = null;
            this._selfLayout = null;
            this._commonLayout = null;
            this._orderCollection = null;
            this._horizontalPosition = undefined;
            this._partWidth = undefined;
            this._partHeight = undefined;
            _super.prototype.dispose.call(this);
        };
        Column.paddingX = 10;
        Column.paddingY = 10;
        Column.gapX = 10;
        Column.gapY = 10;
        return Column;
    }(BaseControl_5.BaseControl));
    exports.Column = Column;
});
define("controls/PageNavigator", ["require", "exports", "controls/BaseControl", "controls/AdvancedButton"], function (require, exports, BaseControl_6, AdvancedButton_1) {
    "use strict";
    exports.__esModule = true;
    var PageNavigatorEventTypes;
    (function (PageNavigatorEventTypes) {
        PageNavigatorEventTypes["PREVIOUS_PAGE"] = "page-navigator-previous-page";
        PageNavigatorEventTypes["NEXT_PAGE"] = "page-navigator-next-page";
    })(PageNavigatorEventTypes = exports.PageNavigatorEventTypes || (exports.PageNavigatorEventTypes = {}));
    var PageNavigator = (function (_super) {
        __extends(PageNavigator, _super);
        function PageNavigator() {
            var _this = _super.call(this) || this;
            _this.addClass('page-navigator');
            _this._prevButton = new AdvancedButton_1.AdvancedButton();
            _this.appendChild(_this._prevButton);
            _this._prevButton.get_iconRenderer().addClass('icon-page-navigator__arrow-up');
            _this.$prevButton_handler = function (e) {
                e.stopImmediatePropagation();
                _this.dispatchEventWith(PageNavigatorEventTypes.PREVIOUS_PAGE, true, true);
            };
            _this._prevButton.addEventListener('click', _this.$prevButton_handler);
            _this._prevButton.set_label("пред-я страница");
            _this._indicator = new BaseControl_6.BaseControl();
            _this._indicator.addClass('indicator');
            _this.appendChild(_this._indicator);
            _this._nextButton = new AdvancedButton_1.AdvancedButton();
            _this.appendChild(_this._nextButton);
            _this._nextButton.get_iconRenderer().addClass('icon-page-navigator__arrow-down');
            _this.$nextButton_handler = function (e) {
                e.stopImmediatePropagation();
                _this.dispatchEventWith(PageNavigatorEventTypes.NEXT_PAGE, true, true);
            };
            _this._nextButton.addEventListener('click', _this.$nextButton_handler);
            _this._nextButton.set_label("след-я страница");
            return _this;
        }
        PageNavigator.prototype.setIndicatorText = function (text) {
            this._indicator.get_element().innerHTML = text;
        };
        PageNavigator.prototype.dispose = function () {
            this._prevButton.removeEventListener('click', this.$prevButton_handler);
            this.$prevButton_handler = null;
            this.removeChild(this._prevButton);
            this._prevButton.dispose();
            this._prevButton = null;
            this._nextButton.removeEventListener('click', this.$nextButton_handler);
            this.$nextButton_handler = null;
            this.removeChild(this._nextButton);
            this._nextButton.dispose();
            this._nextButton = null;
            this.removeChild(this._indicator);
            this._indicator.dispose();
            this._indicator = null;
            _super.prototype.dispose.call(this);
        };
        return PageNavigator;
    }(BaseControl_6.BaseControl));
    exports.PageNavigator = PageNavigator;
});
define("controls/ControlPanel", ["require", "exports", "controls/BaseControl", "controls/PageNavigator", "controls/AdvancedButton"], function (require, exports, BaseControl_7, PageNavigator_1, AdvancedButton_2) {
    "use strict";
    exports.__esModule = true;
    var ControlPanel = (function (_super) {
        __extends(ControlPanel, _super);
        function ControlPanel() {
            var _this = _super.call(this) || this;
            _this.addClass('control-panel');
            _this._logo = new BaseControl_7.BaseControl();
            _this._logo.addClass('logo');
            _this._logo_r = new BaseControl_7.BaseControl();
            _this._logo_r.addClass('icon-control-panel__logo__r');
            _this._logo.appendChild(_this._logo_r);
            _this._logo_ = new BaseControl_7.BaseControl();
            _this._logo_.addClass('icon-control-panel__logo');
            _this._logo.appendChild(_this._logo_);
            _this._logo_k = new BaseControl_7.BaseControl();
            _this._logo_k.addClass('icon-control-panel__logo__k');
            _this._logo.appendChild(_this._logo_k);
            _this.appendChild(_this._logo);
            _this._time = new BaseControl_7.BaseControl();
            _this._time.addClass('time');
            _this.appendChild(_this._time);
            _this._pageNavigator = new PageNavigator_1.PageNavigator();
            _this.appendChild(_this._pageNavigator);
            _this._rollbackContainer = new BaseControl_7.BaseControl();
            _this._rollbackContainer.addClass('rollback-container');
            _this.appendChild(_this._rollbackContainer);
            _this._rollback = new AdvancedButton_2.AdvancedButton();
            _this._rollback.addClass('rollback');
            _this._rollbackContainer.appendChild(_this._rollback);
            _this._rollback.get_iconRenderer().addClass('icon-rollback');
            _this.$rollback_handler = function (e) {
                e.stopImmediatePropagation();
                console.log('rollback');
            };
            _this._rollback.addEventListener('click', _this.$rollback_handler);
            _this._rollback.set_label("отмена операции");
            return _this;
        }
        ControlPanel.prototype.setPageNavigatorText = function (text) {
            this._pageNavigator.setIndicatorText(text);
        };
        ControlPanel.prototype.tick = function (dt) {
            this._time.set_text(moment(dt).format('HH:mm'));
        };
        ControlPanel.prototype.dispose = function () {
            this._logo.removeChild(this._logo_r);
            this._logo_r.dispose();
            this._logo_r = null;
            this._logo.removeChild(this._logo_);
            this._logo_.dispose();
            this._logo_ = null;
            this._logo.removeChild(this._logo_k);
            this._logo_k.dispose();
            this._logo_k = null;
            this.removeChild(this._logo);
            this._logo.dispose();
            this._logo = null;
            this.removeChild(this._time);
            this._time.dispose();
            this._time = null;
            this.removeChild(this._pageNavigator);
            this._pageNavigator.dispose();
            this._pageNavigator = null;
            this._rollback.removeEventListener('click', this.$rollback_handler);
            this.$rollback_handler = null;
            this._rollbackContainer.removeChild(this._rollback);
            this._rollback.dispose();
            this._rollback = null;
            this.removeChild(this._rollbackContainer);
            this._rollbackContainer.dispose();
            this._rollbackContainer = null;
            _super.prototype.dispose.call(this);
        };
        return ControlPanel;
    }(BaseControl_7.BaseControl));
    exports.ControlPanel = ControlPanel;
});
define("controls/OrderStage", ["require", "exports", "controls/BaseControl", "controls/Column", "controls/ControlPanel", "controls/PageNavigator", "controls/OrderItemRenderer", "core/Settings"], function (require, exports, BaseControl_8, Column_1, ControlPanel_1, PageNavigator_2, OrderItemRenderer_2, Settings_1) {
    "use strict";
    exports.__esModule = true;
    var OrderStage = (function (_super) {
        __extends(OrderStage, _super);
        function OrderStage(_screenData) {
            var _this = _super.call(this) || this;
            _this._screenData = _screenData;
            _this._currentPageIndex = 0;
            _this._numPages = 0;
            _this._maxScrollHeight = 0;
            _this._partWidth = 0;
            _this._partHeight = 0;
            _this._columns = [];
            _this.addClass('order-stage');
            _this._controlPanel = new ControlPanel_1.ControlPanel();
            _this._element.appendChild(_this._controlPanel.get_element());
            _this.$prevPage_handler = function (e) {
                e.stopImmediatePropagation();
                _this.prevPage();
            };
            _this._controlPanel.addEventListener(PageNavigator_2.PageNavigatorEventTypes.PREVIOUS_PAGE, _this.$prevPage_handler);
            _this.$nextPage_handler = function (e) {
                e.stopImmediatePropagation();
                _this.nextPage();
            };
            _this._controlPanel.addEventListener(PageNavigator_2.PageNavigatorEventTypes.NEXT_PAGE, _this.$nextPage_handler);
            _this.$selectOrder_handler = function (e) {
                var order = e.data;
                var orderModel = order.get_data();
                var n = orderModel.state + 1;
                if (n > 4)
                    n = 1;
                orderModel.state = n;
                for (var _i = 0, _a = orderModel.orderlines; _i < _a.length; _i++) {
                    var ol = _a[_i];
                    ol.state = n;
                }
                order.applyStyle();
                for (var _b = 0, _c = _this._columns; _b < _c.length; _b++) {
                    var c = _c[_b];
                    c.deletePiece();
                    c.updateLayout();
                }
                console.log('select order: ' + orderModel.guid);
            };
            _this.addEventListener(OrderItemRenderer_2.OrderItemRendererEventTypes.SELECT, _this.$selectOrder_handler);
            _this._masked = new BaseControl_8.BaseControl();
            _this.appendChild(_this._masked);
            _this._masked.addClass('masked');
            _this._layout = new BaseControl_8.BaseControl();
            _this._masked.appendChild(_this._layout);
            _this._layout.addClass('layout');
            return _this;
        }
        OrderStage.prototype.get_orderCollection = function () { return this._orderCollection; };
        OrderStage.prototype.set_orderCollection = function (v) {
            if (this._orderCollection != v) {
                this._orderCollection = v;
                for (var _i = 0, _a = this._columns; _i < _a.length; _i++) {
                    var column = _a[_i];
                    column.set_orderCollection(this._orderCollection);
                }
                this.resetSize();
            }
        };
        OrderStage.prototype.resetOrderCollection = function (orders) {
            var collection = {};
            for (var _i = 0, orders_1 = orders; _i < orders_1.length; _i++) {
                var orderData = orders_1[_i];
                if (!collection.hasOwnProperty(orderData.tag))
                    collection[orderData.tag] = [];
                collection[orderData.tag].push(orderData);
            }
            for (var tag in this._screenData.tagList) {
                if (!collection.hasOwnProperty(tag))
                    collection[tag] = [];
            }
            if (this._orderCollection) {
                for (var k in this._orderCollection) {
                    this._orderCollection[k] = null;
                    delete this._orderCollection[k];
                }
            }
            this.set_orderCollection(collection);
        };
        OrderStage.prototype.createColumns = function () {
            this.calculateParts();
            var maxHeight = 0;
            for (var _i = 0, _a = this._screenData.layout.columns; _i < _a.length; _i++) {
                var columnLayout = _a[_i];
                var column = new Column_1.Column(columnLayout, this._screenData.layout.columns, this._orderCollection, Settings_1.Settings.instance.get_statesMap());
                this._layout.appendChild(column);
                this._columns.push(column);
                column.setExplicitSize(this._partWidth, this._partHeight);
                maxHeight = Math.max(column.get_height(), maxHeight);
            }
            this._maxScrollHeight = maxHeight;
            this._layout.set_height(maxHeight);
            this.calculateScrollParams();
            this.updatePageNavigatorText();
            this.scrollToPage(this._currentPageIndex);
        };
        OrderStage.prototype.calculateParts = function () {
            this._partWidth = this._masked.get_width() / this._screenData.layout.totalParts;
            this._partHeight = this._masked.get_height();
        };
        OrderStage.prototype.calculateScrollParams = function () {
            this._numPages = this._maxScrollHeight / this._partHeight;
            var lastPageIndex = this._currentPageIndex;
            if (this._currentPageIndex >= this._numPages) {
                this._currentPageIndex = this._numPages > 1 ? this._numPages - 1 : 0;
                this.updatePageNavigatorText();
                this.scrollToPage(this._currentPageIndex);
            }
        };
        OrderStage.prototype.updatePageNavigatorText = function () {
            this._controlPanel.setPageNavigatorText((this._currentPageIndex + 1) + "/" + this._numPages);
        };
        OrderStage.prototype.prevPage = function () {
            if (this._currentPageIndex > 0) {
                this._currentPageIndex--;
                this.updatePageNavigatorText();
                this.scrollToPage(this._currentPageIndex);
            }
        };
        OrderStage.prototype.nextPage = function () {
            if (this._currentPageIndex < this._numPages - 1 && this._numPages > 1) {
                this._currentPageIndex++;
                this.updatePageNavigatorText();
                this.scrollToPage(this._currentPageIndex);
            }
        };
        OrderStage.prototype.scrollToPage = function (index) {
            this._layout.set_y(-index * this._partHeight);
        };
        OrderStage.prototype.tick = function (dt) {
            this._controlPanel.tick(dt);
            if (this._columns) {
                for (var _i = 0, _a = this._columns; _i < _a.length; _i++) {
                    var column = _a[_i];
                    column.tick(dt);
                }
            }
        };
        OrderStage.prototype.resetSize = function () {
            this.calculateParts();
            var maxHeight = 0;
            for (var _i = 0, _a = this._columns; _i < _a.length; _i++) {
                var column = _a[_i];
                column.setExplicitSize(this._partWidth, this._partHeight);
                maxHeight = Math.max(column.get_height(), maxHeight);
            }
            this._maxScrollHeight = maxHeight;
            this._layout.set_height(maxHeight);
            this.calculateScrollParams();
            this.updatePageNavigatorText();
            this.scrollToPage(this._currentPageIndex);
        };
        OrderStage.prototype.dispose = function () {
            for (var _i = 0, _a = this._columns; _i < _a.length; _i++) {
                var column = _a[_i];
                this._layout.removeChild(column);
                column.dispose();
                column = null;
            }
            this._columns = null;
            this._masked.removeChild(this._layout);
            this._layout = null;
            this.removeChild(this._masked);
            this._masked = null;
            if (this._orderCollection) {
                for (var k in this._orderCollection) {
                    this._orderCollection[k] = null;
                    delete this._orderCollection[k];
                }
                this._orderCollection = null;
            }
            _super.prototype.dispose.call(this);
        };
        return OrderStage;
    }(BaseControl_8.BaseControl));
    exports.OrderStage = OrderStage;
});
define("controls/InputText", ["require", "exports", "controls"], function (require, exports, controls_1) {
    "use strict";
    exports.__esModule = true;
    var InputText = (function (_super) {
        __extends(InputText, _super);
        function InputText() {
            var _this = _super.call(this, 'input') || this;
            _this._input = _this._element;
            return _this;
        }
        InputText.prototype.get_text = function () {
            return this._input.value;
        };
        InputText.prototype.set_text = function (value) {
            this._input.value = value;
        };
        InputText.prototype.dispose = function () {
            this._input = null;
            _super.prototype.dispose.call(this);
        };
        return InputText;
    }(controls_1.BaseControl));
    exports.InputText = InputText;
});
define("controls/PopupManager", ["require", "exports"], function (require, exports) {
    "use strict";
    exports.__esModule = true;
    var PopupEventTypes;
    (function (PopupEventTypes) {
        PopupEventTypes["CLOSE"] = "popup-close";
    })(PopupEventTypes = exports.PopupEventTypes || (exports.PopupEventTypes = {}));
    var PopupManager = (function () {
        function PopupManager(_name, _root) {
            this._name = _name;
            this._root = _root;
            this._popups = [];
            if (!PopupManager._pull[name]) {
                PopupManager._pull[name] = this;
            }
        }
        PopupManager.show = function (name, control) {
            if (PopupManager._pull.hasOwnProperty(name)) {
                var manager = PopupManager._pull[name];
                if (manager)
                    manager.show(control);
            }
        };
        PopupManager.prototype.show = function (control) {
            var _this = this;
            var $popupClose_handler = function (e) {
                control.removeEventListener(PopupEventTypes.CLOSE, $popupClose_handler);
                _this.removePopup(control);
            };
            control.addEventListener(PopupEventTypes.CLOSE, $popupClose_handler);
            this._popups.push(control);
            this._root.appendChild(control);
        };
        PopupManager.prototype.removePopup = function (popup) {
            this._root.removeChild(popup);
            popup.dispose();
            popup = null;
        };
        PopupManager.prototype.removePopups = function () {
            while (this._popups.length > 0) {
                var popup = this._popups.pop();
                this.removePopup(popup);
            }
        };
        PopupManager.prototype.removeManagerByName = function (name) {
        };
        PopupManager.prototype.dispose = function () {
            delete PopupManager._pull[name];
            this._root = null;
        };
        PopupManager._pull = {};
        return PopupManager;
    }());
    exports.PopupManager = PopupManager;
});
define("controls/BasePopup", ["require", "exports", "controls/BaseControl", "controls/PopupManager"], function (require, exports, BaseControl_9, PopupManager_1) {
    "use strict";
    exports.__esModule = true;
    var BasePopup = (function (_super) {
        __extends(BasePopup, _super);
        function BasePopup() {
            var _this = _super.call(this) || this;
            _this.addClass('popup');
            return _this;
        }
        BasePopup.prototype.close = function () {
            this.dispatchEventWith(PopupManager_1.PopupEventTypes.CLOSE, true);
        };
        return BasePopup;
    }(BaseControl_9.BaseControl));
    exports.BasePopup = BasePopup;
});
define("controls/LoginPopup", ["require", "exports", "controls/BasePopup", "controls/InputText"], function (require, exports, BasePopup_1, InputText_1) {
    "use strict";
    exports.__esModule = true;
    var LoginPopup = (function (_super) {
        __extends(LoginPopup, _super);
        function LoginPopup() {
            var _this = _super.call(this) || this;
            _this.addClass("login");
            _this._input = new InputText_1.InputText();
            _this.appendChild(_this._input);
            return _this;
        }
        LoginPopup.prototype.dispose = function () {
            _super.prototype.dispose.call(this);
        };
        return LoginPopup;
    }(BasePopup_1.BasePopup));
    exports.LoginPopup = LoginPopup;
});
define("controls", ["require", "exports", "controls/AdvancedButton", "controls/BaseControl", "controls/Button", "controls/Column", "controls/ControlPanel", "controls/OrderItemRenderer", "controls/OrderStage", "controls/PageNavigator", "controls/ProductItemRenderer", "controls/InputText", "controls/LoginPopup"], function (require, exports, AdvancedButton_3, BaseControl_10, Button_2, Column_2, ControlPanel_2, OrderItemRenderer_3, OrderStage_1, PageNavigator_3, ProductItemRenderer_2, InputText_2, LoginPopup_1) {
    "use strict";
    function __export(m) {
        for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
    }
    exports.__esModule = true;
    __export(AdvancedButton_3);
    __export(BaseControl_10);
    __export(Button_2);
    __export(Column_2);
    __export(ControlPanel_2);
    __export(OrderItemRenderer_3);
    __export(OrderStage_1);
    __export(PageNavigator_3);
    __export(ProductItemRenderer_2);
    __export(InputText_2);
    __export(LoginPopup_1);
});
define("core/DataService", ["require", "exports", "data", "events", "core"], function (require, exports, data_2, events_3, core_1) {
    "use strict";
    exports.__esModule = true;
    var DataService = (function (_super) {
        __extends(DataService, _super);
        function DataService() {
            var _this = _super.call(this) || this;
            _this._count = 0;
            _this._loaders = [];
            return _this;
        }
        DataService.prototype.get_token = function () {
            return core_1.Settings && core_1.Settings.instance ? core_1.Settings.instance.get_data().token : undefined;
        };
        DataService.prototype.getSettings = function (cb) {
            this.loadJSONRes('assets/dummy/getSettings.json', null, cb);
        };
        DataService.prototype.getOrdersDemo = function (cb) {
            this._count += Number(1);
            if (this._count > 2)
                this._count = 0;
            this.loadJSONRes('assets/dummy/getOrders' + String(this._count) + '.json', null, cb);
        };
        DataService.prototype.getOrders = function (screen, cb) {
            this.loadJSONRes('assets/dummy/getOrders.json', [{ key: "screen", value: String(screen) }], cb);
        };
        DataService.prototype.loadJSONRes = function (resource, params, cb) {
            var _this = this;
            var dataLoader = new data_2.DataLoader(this.get_token());
            dataLoader.set_contentType(data_2.DataLoaderContentType.JSON);
            var $data_handler = function (e) {
                cb(e.get_data());
                _this.removeLoader(dataLoader);
            };
            var $error_handler = function (e) {
                console.log(e);
                _this.removeLoader(dataLoader);
            };
            dataLoader.set_params(params);
            dataLoader.addEventListener(data_2.DataLoaderEventTypes.DATA, $data_handler);
            dataLoader.addEventListener(data_2.DataLoaderEventTypes.ERROR, $error_handler);
            this._loaders.push(dataLoader);
            dataLoader.set_source(resource);
            dataLoader.load();
        };
        DataService.prototype.removeLoader = function (dataLoader) {
            var index = this._loaders.indexOf(dataLoader);
            if (index > -1) {
                this._loaders.splice(index, 1);
                dataLoader.dispose();
                dataLoader = null;
            }
        };
        DataService.prototype.removeAllLoaders = function () {
            for (var _i = 0, _a = this._loaders; _i < _a.length; _i++) {
                var dataLoader = _a[_i];
                dataLoader.dispose();
                dataLoader = null;
            }
            this._loaders = null;
        };
        DataService.prototype.dispose = function () {
            this.removeAllLoaders();
            _super.prototype.dispose.call(this);
        };
        return DataService;
    }(events_3.EventDispatcher));
    exports.DataService = DataService;
});
define("core/SocketService", ["require", "exports", "core"], function (require, exports, core_2) {
    "use strict";
    exports.__esModule = true;
    var SocketServiceMessageTypes;
    (function (SocketServiceMessageTypes) {
        SocketServiceMessageTypes["OPEN"] = "open";
        SocketServiceMessageTypes["CLOSE"] = "close";
        SocketServiceMessageTypes["ERROR"] = "error";
        SocketServiceMessageTypes["PING"] = "PING";
        SocketServiceMessageTypes["UPDATE"] = "UPDATE";
        SocketServiceMessageTypes["LOGOUT"] = "LOGOUT";
    })(SocketServiceMessageTypes || (SocketServiceMessageTypes = {}));
    var SocketService = (function () {
        function SocketService(_onUpdate, _onLogout, _onOpen, _onClose, _onError) {
            this._onUpdate = _onUpdate;
            this._onLogout = _onLogout;
            this._onOpen = _onOpen;
            this._onClose = _onClose;
            this._onError = _onError;
            this.createSocket();
        }
        SocketService.prototype.createSocket = function () {
            var _this = this;
            this._socket = new WebSocket("ws://" + location.host + "/api/eventz", []);
            this.$socket_openHandler = function (e) {
                console.log("Открылся сокет", event);
                if (_this._onOpen)
                    _this._onOpen();
                _this.ping();
            };
            this._socket.addEventListener(SocketServiceMessageTypes.OPEN, this.$socket_openHandler);
            this.$socket_closeHandler = function (e) {
                _this.stopPing();
                console.log("Закрылся сокет", event);
                if (_this._onClose)
                    _this._onClose();
            };
            this._socket.addEventListener(SocketServiceMessageTypes.CLOSE, this.$socket_closeHandler);
            this.$socket_errorHandler = function (e) {
                console.log("Ошибка на сокете", event);
                if (_this._onError)
                    _this._onError();
            };
            this._socket.addEventListener(SocketServiceMessageTypes.ERROR, this.$socket_errorHandler);
            this.$socket_updateHandler = function (e) {
                console.log("Обновление заказов", event);
                if (_this._onUpdate)
                    _this._onUpdate();
            };
            this._socket.addEventListener(SocketServiceMessageTypes.UPDATE, this.$socket_updateHandler);
            this.$socket_logoutHandler = function (e) {
                console.log("Логаут", event);
                if (_this._onLogout)
                    _this._onLogout();
            };
            this._socket.addEventListener(SocketServiceMessageTypes.LOGOUT, this.$socket_logoutHandler);
        };
        SocketService.prototype.ping = function () {
            var _this = this;
            this._isPingRun = true;
            this._timerHanlerNum = setTimeout(function () {
                _this.send(core_2.Settings.instance.get_data().token + SocketServiceMessageTypes.PING);
                _this.ping();
            }, 1000);
        };
        SocketService.prototype.stopPing = function () {
            this._isPingRun = false;
            clearTimeout(this._timerHanlerNum);
        };
        SocketService.prototype.removeSocket = function () {
            this.stopPing();
            if (this._socket) {
                this._socket.removeEventListener(SocketServiceMessageTypes.OPEN, this.$socket_openHandler);
                this._socket.removeEventListener(SocketServiceMessageTypes.CLOSE, this.$socket_closeHandler);
                this._socket.removeEventListener(SocketServiceMessageTypes.ERROR, this.$socket_errorHandler);
                this._socket.removeEventListener(SocketServiceMessageTypes.UPDATE, this.$socket_updateHandler);
                this._socket.removeEventListener(SocketServiceMessageTypes.LOGOUT, this.$socket_logoutHandler);
                this._socket.close();
                this._socket = null;
            }
        };
        SocketService.prototype.send = function (message) {
            this._socket.send(message);
        };
        SocketService.prototype.dispose = function () {
            this.removeSocket();
            this._onClose = null;
            this._onError = null;
            this._onLogout = null;
            this._onOpen = null;
            this._onUpdate = null;
        };
        return SocketService;
    }());
    exports.SocketService = SocketService;
});
define("core", ["require", "exports", "core/DataService", "core/Settings", "core/SocketService"], function (require, exports, DataService_1, Settings_2, SocketService_1) {
    "use strict";
    function __export(m) {
        for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
    }
    exports.__esModule = true;
    __export(DataService_1);
    __export(Settings_2);
    __export(SocketService_1);
});
define("types/PopupTypes", ["require", "exports"], function (require, exports) {
    "use strict";
    exports.__esModule = true;
    var PopupTypes;
    (function (PopupTypes) {
        PopupTypes["MAIN"] = "main";
    })(PopupTypes = exports.PopupTypes || (exports.PopupTypes = {}));
});
define("types", ["require", "exports", "types/PopupTypes"], function (require, exports, PopupTypes_1) {
    "use strict";
    function __export(m) {
        for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
    }
    exports.__esModule = true;
    __export(PopupTypes_1);
});
define("Main", ["require", "exports", "controls", "core", "cross-browser", "controls/PopupManager", "types"], function (require, exports, controls_2, core_3, cross_browser_2, PopupManager_2, types_1) {
    "use strict";
    exports.__esModule = true;
    var Main = (function (_super) {
        __extends(Main, _super);
        function Main() {
            var _this = _super.call(this) || this;
            _this._isInitialized = false;
            _this._lastTimestamp = 0;
            _this._currentScreenId = 0;
            _this.addClass("main");
            _this._stage = new controls_2.BaseControl();
            _this._stage.addClass('stage');
            _this.appendChild(_this._stage);
            _this._popupStage = new controls_2.BaseControl();
            _this._popupStage.addClass('popup-stage');
            _this.appendChild(_this._popupStage);
            new PopupManager_2.PopupManager(types_1.PopupTypes.MAIN, _this._popupStage);
            PopupManager_2.PopupManager.show(types_1.PopupTypes.MAIN, new controls_2.LoginPopup());
            return _this;
        }
        Main.prototype.updateOrder = function () {
            var _this = this;
            this._service.getOrders(this._currentScreenId, function (resp) {
                _this._orderStage.resetOrderCollection(resp.data.orders);
                resp = null;
            });
        };
        Main.prototype.logout = function () {
        };
        Main.prototype.test = function () {
            var _this = this;
            setTimeout(function () {
                _this._service.getOrdersDemo(function (resp) {
                    _this.getOrdersDemo();
                    resp = null;
                });
            }, 5000);
        };
        Main.prototype.getOrdersDemo = function () {
            var _this = this;
            this._service.getOrdersDemo(function (resp) {
                _this._orderStage.resetOrderCollection(resp.data.orders);
                if (!_this._isInitialized && _this._orderStage.get_orderCollection()) {
                    _this.initialize();
                }
            });
        };
        Main.prototype.getOrders = function () {
            var _this = this;
            this._service.getOrders(this._currentScreenId, function (resp) {
                _this._orderStage.resetOrderCollection(resp.data.orders);
                if (!_this._isInitialized && _this._orderStage.get_orderCollection()) {
                    _this.initialize();
                }
            });
        };
        Main.prototype.createOrderStage = function (screenId) {
            this._orderStage = new controls_2.OrderStage(core_3.Settings.instance.get_data().orderStage[screenId]);
            this._stage.appendChild(this._orderStage);
        };
        Main.prototype.runTimer = function () {
            var _this = this;
            this.tick();
            setTimeout(function () {
                _this.runTimer();
            }, 1000);
        };
        Main.prototype.tick = function () {
            var dt = new Date();
            this._orderStage.tick(dt);
        };
        Main.prototype.initialize = function () {
            var _this = this;
            this.$resizeHandler = function (e) {
                _this._orderStage.resetSize();
            };
            document.getElementsByTagName('body')[0].appendChild(this._element);
            cross_browser_2.addEventListener(window, 'resize', this.$resizeHandler);
            this.runTimer();
            this._orderStage.createColumns();
        };
        Main.prototype.dispose = function () {
            document.getElementsByTagName('body')[0].removeChild(this._element);
            this._element = null;
            cross_browser_2.removeEventListener(window, 'resize', this.$resizeHandler);
            this.$resizeHandler = null;
            _super.prototype.dispose.call(this);
        };
        return Main;
    }(controls_2.BaseControl));
    exports.Main = Main;
});
define("index", ["require", "exports", "Main"], function (require, exports, Main_1) {
    "use strict";
    exports.__esModule = true;
    new Main_1.Main();
});
