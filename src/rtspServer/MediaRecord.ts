import * as fs from "fs";
import * as path from "path";
import * as ffmpeg from "fluent-ffmpeg";
import { EventEmitter } from "events";
import { ICameraSettings, Settings, IH264Quality, IOutputSettings } from "../settings";
import { logger } from "../logger";
import { MediaRecordEventTypes } from "./MediaRecordEventTypes";

import { mkdirp, getFilesSortedByCDate, getBytesByStr, getDirectorySize } from "../utils/FSUtil";
import { App } from "../app";

export class MediaRecord extends EventEmitter {

    private static WATCH_RECONNECTION_TIMEOUT: number = 600000;

    public static count: number = 0;

    public static F_PREVIEW_SMALL: string = "m";
    //public static F_PREVIEW_HIRES: string = "h";
    public static F_FULL: string = "f";

    public static MAX_PREVIEW_BUF_FILES: number = 10;
    //private static WATCHER_TIMEOUT: number = 6000;

    private static FF_SIGKILL: string = "SIGKILL";
    //private static DEFAULT_OUTPUT_FOLDER: string = "out" + path.sep;

    //private _output: string;

    private _process: ffmpeg.FfmpegCommand;

    //private _capacity: number = 0;

    private _ns: string;

    private _watchReconnectionTmr: NodeJS.Timer;

    public get camDebugName(): string {
        return "[" + this._name + "::" + this._ns + "]";
    }

    constructor(private _name: string, private _source: string, private _config: IOutputSettings, private _type: number) {
        super();

        this.createNS();

        MediaRecord.count++;
        
        this.run();
    }

    private createNS() {
        switch (this._type) {
            case 0: {
                this._ns = MediaRecord.F_FULL;
                break;
            }
            case 1: {
                this._ns = MediaRecord.F_PREVIEW_SMALL;
                break;
            }
        }
    }

    private run(): void {

        const output = this.getOutputPath();
        const params = this.getOutputParams();

        this._process = ffmpeg();
        this._process
            .setFfmpegPath(Settings.ffmpeg.ffmpegPath)
            .setFfprobePath(Settings.ffmpeg.ffprobePath)
            .addInput(this._source)
            .on("exit", (code: string, signal: string) => {
                logger.info(`${this.camDebugName} exit`);
            })
            .on("start", (ffmpegCommand: any) => {
                logger.info(`${this.camDebugName} Connecting...`)
                this.watchReconnection();
                //this.emit(MediaRecordEventTypes.START);
            })
            .on("progress", (data: any) => {
                this.stopWatchReconnectionTmr();
                logger.info(`${this.camDebugName} ${data.frames}`);
                //this.emit(MediaRecordEventTypes.PROGRESS);
            })
            .on("end", () => {
                logger.info(`${this.camDebugName} End.`);
                this.emit(MediaRecordEventTypes.FAIL);
                //this.emit(MediaRecordEventTypes.COMPLETE);
            })
            .on("error", (error: any) => {
                logger.error(new Error(`${this.camDebugName} Error on write stream.\n${this._name}: ${error.message}`));
                this.emit(MediaRecordEventTypes.FAIL);
            })
            .outputOptions(params)
            .output(output)
            .run();
    }

    private watchReconnection(): void {
        this.stopWatchReconnectionTmr();
        this._watchReconnectionTmr = setTimeout(() => {
            this.emit(MediaRecordEventTypes.FAIL);
        }, MediaRecord.WATCH_RECONNECTION_TIMEOUT);
    }

    private stopWatchReconnectionTmr(): void {
        if (this._watchReconnectionTmr) {
            clearTimeout(this._watchReconnectionTmr);
            this._watchReconnectionTmr = null;
        }
    }

    private getOutputPath(): string {
        return `http://${Settings.translatorServer.address}/${Settings.uuid}/${this._name}/${this._ns}/${this._type == 0 ? '__f' : ''}/index.m3u8`;
    }

    private getOutputParams(): Array<any> {
        let params: Array<any>;
        switch (this._type) {
            case 0: {
                params = [
                    "-r 24",
                    "-c copy",
                    "-s 1280x720",
                    "-an",
                    "-b:v 800k",
                    "-maxrate 1200k",
                    "-bufsize 1800k",
                    `-keyint_min 48`,
                    //`-crf 10`,
                    "-f hls",
                    "-use_localtime 1",
                    //"-hls_playlist_type vod",
                    "-hls_time 5",
                    "-hls_allow_cache 0",
                    "-hls_list_size 3",
                    "-hls_flags omit_endlist",
                    //"-use_localtime 1",
                    `-hls_base_url http://${Settings.translatorServer.address}/${Settings.uuid}/${this._name}/${this._ns}/__f/`,
                    "-method PUT"];
                break;
            }
            case 1: {
                params = [
                    "-r 24",
                    "-preset ultrafast",
                    "-c copy",
                    "-s 842x480",
                    "-an",
                    "-b:v 20k",
                    "-maxrate 64k",
                    "-bufsize 512k",
                    `-keyint_min 5`,
                    `-crf 5`,
                    /*"-g 30",
                    "-r 1",*/
                    "-f hls",
                    //"-hls_playlist_type vod",
                    "-hls_time 1",
                    "-hls_list_size 5",
                    "-hls_allow_cache 0",
                    "-hls_flags omit_endlist",
                    "-hls_flags delete_segments",
                    //"-use_localtime 1",
                    `-hls_base_url http://${Settings.translatorServer.address}/${Settings.uuid}/${this._name}/${this._ns}/`,
                    "-method PUT"
                ];
                break;
            }
        }
        return params;
    }

    public dispose(): void {
        this.stopWatchReconnectionTmr();

        logger.warn(`${this.camDebugName} End.`);

        this.removeAllListeners();
        if (this._process) {
            this._process.kill(MediaRecord.FF_SIGKILL);
            this._process = null;
        }
    }
}