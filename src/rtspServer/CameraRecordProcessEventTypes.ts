// types
export enum CameraRecordProcessEventTypes {
    START = "cam-record-start",
    PROGRESS = "cam-record-progress", 
    COMPLETE = "cam-record-complete",
    FAIL = "cam-record-fail"
}