export enum MediaRecordEventTypes {
    START = "media-stream-record-start",
    PROGRESS = "media-stream-record-progress",
    COMPLETE = "media-stream-record-complete",
    FAIL = "media-stream-record-fail"
}