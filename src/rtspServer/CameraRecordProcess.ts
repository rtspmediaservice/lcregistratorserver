import { EventEmitter } from "events";
import { ICameraSource } from "../settings";
import { MediaRecord } from "./MediaRecord";
import { MediaRecordEventTypes } from "./MediaRecordEventTypes";
import { clearTimeout } from "timers";

export class CameraRecordProcess extends EventEmitter {

    private static RECONNECT_TIMEOUT: number = 1000;

    public get name(): string {
        return this._name;
    }

    private _mediaRecordProcess: MediaRecord;

    private _reconnectTimer: NodeJS.Timer;

    constructor(private _name: string, private _config: ICameraSource, private _type: number) { super(); }

    private record(): void {
        
        if (!this._mediaRecordProcess) {
            this._mediaRecordProcess = new MediaRecord(this._name, this._config.src, this._config.output, this._type);
            this._mediaRecordProcess.addListener(MediaRecordEventTypes.COMPLETE, () => { this.reconnect(); });
            this._mediaRecordProcess.addListener(MediaRecordEventTypes.FAIL, () => { this.reconnect(); })
        }
    }

    private reconnect(timeout?: number): void {
        this.clearTimer();
        this.stopRecord();
        let t: number = (timeout ? timeout : CameraRecordProcess.RECONNECT_TIMEOUT);
        t += Number(Math.random() * 100);
        this._reconnectTimer = setTimeout(() => {
            this.record();
        }, t);
    }

    private stopRecord(): void {
        if (this._mediaRecordProcess) {
            this._mediaRecordProcess.dispose();
            this._mediaRecordProcess = null;
        }
        this.clearTimer();
    }

    private clearTimer(): void {
        if (this._reconnectTimer) {
            clearTimeout(this._reconnectTimer);
            this._reconnectTimer = null;
        }
    }

    public start(autoReconnect: boolean = false): void {
        if (autoReconnect) this.reconnect();
        else this.record();
    }

    public stop(): void {
        this.stopRecord();
    }

    public dispose(): void {
        this.stopRecord();
    }
}