import { EventEmitter } from "events";

import { Settings } from "../settings";
import { logger } from "../logger";
import { CameraRecordProcess } from "./CameraRecordProcess";
import { CameraRecordProcessEventTypes } from "./CameraRecordProcessEventTypes";
import { MediaRecord } from "./MediaRecord";
import { TranslatorApi } from "../translatorApi";
import * as path from "path";

export class CamProcesses {
    public full: CameraRecordProcess;
    public previewM: CameraRecordProcess;
    //public previewH: CameraRecordProcess;

    public dispose(): void {
        if (this.full) {
            this.full.removeAllListeners();
            this.full.dispose();
            this.full = null;
        }
        if (this.previewM) {
            this.previewM.removeAllListeners();
            this.previewM.dispose();
            this.previewM = null;
        }
        /*if (this.previewH) {
            this.previewH.removeAllListeners();
            this.previewH.dispose();
            this.previewH = null;
        }*/
    }
}

export class RtspServer extends EventEmitter {

    public static instance: RtspServer;

    //private static RPC_RECONNECT_TIMEOUT: number = 1000;

    private _recordProcesses: Map<string, CamProcesses> = new Map();
    public has(camName: string): boolean {
        return this._recordProcesses.has(camName);
    }
    public get(camName: string): CamProcesses {
        return this._recordProcesses.has(camName) ? this._recordProcesses.get(camName) : null;
    }
    /*public getCamList(): Array<string> {
        let list: Array<string> = [];
        this._recordProcesses.forEach((v, k, map) => {
            list.push(k);
        });
        return list;
    }
    public getSnapshot(camName: string): string {
        const cam = this._recordProcesses.get(camName);
        if (cam) {
            cam.preview.getBase64Snapshot((image: string) => {
                return image;
            })
        }
        return null;
    }*/

    constructor() {
        super();
        RtspServer.instance = this;
    }

    public run(): void {
        this.startCamRecords();
    }

    stopAll() {
        this._recordProcesses.forEach((v, k, map) => {
            if (v) v.previewM.stop();
        });
    }

    play(cams: Array<string>, preview: boolean = false) {
        const playCams = () => {
            for (const camName of cams) {
                const camProcesses = RtspServer.instance.get(camName);
                if (camProcesses) {
                    if (preview) camProcesses.previewM.start();
                    //else  camProcesses.previewH.start();
                }
            }
        }
        if (preview) {
            this.deletePreview(Settings.camList())
                .then((data) => {
                    playCams();
                })
                .catch((error) => {
                    logger.error(error);
                    playCams();
                });
        } else playCams();
    }

    stop(cams: Array<string>, preview: boolean = false) {
        const stopCams = () => {
            for (const camName of cams) {
                const camProcesses = RtspServer.instance.get(camName);
                if (camProcesses) {
                    if (preview) camProcesses.previewM.stop();
                    //else  camProcesses.previewH.stop();
                }
            }
        }
        if (preview) {
            this.deletePreview(Settings.camList())
                .then((data) => {
                    stopCams();
                })
                .catch((error) => {
                    stopCams();
                    logger.error(error);
                });
        } else
            stopCams();
    }

    private deletePreview(cams: string[]): Promise<any> {
        return new Promise<any>((resolve, reject) => {
            for (const cam of cams) {
                TranslatorApi.deleteFile(`/${Settings.uuid}/${cam}/m/index.m3u8`, (data, error) => {
                    if (error) reject(error);
                    else resolve(data);
                });
            }
        });
    }

    private startCamRecords(): void {
        if (Settings.cameras == null || Settings.cameras.length == 0) {
            logger.warn("Нет ни одной привязанной камеры!");
            return;
        }
        let num: number = 0;
        for (const cam of Settings.cameras) {
            ++num;
            let isFail: boolean = false;
            if (cam.name == undefined || cam.name == "") {
                logger.warn(`Не задано имя камеры с позицией [${String(num)}]`);
                isFail = true;
            }
            if (this._recordProcesses.has(cam.name)) {
                logger.warn(`${cam.name}::all] Камера с заданным именем уже существует.`);
                isFail = true;
            }
            if (!isFail) {
                const camProcess: CamProcesses = new CamProcesses();
                this._recordProcesses.set(cam.name, camProcess);

                const processF = camProcess.full = new CameraRecordProcess(cam.name, cam.full, 0);

                processF.addListener(CameraRecordProcessEventTypes.FAIL, () => {
                    // При ошибке переконнект к камере
                    logger.info(`[${cam.name}]::${MediaRecord.F_FULL}] Reconnect...`);
                    processF.removeAllListeners();
                    processF.dispose();
                    this.removeProcess(cam.name, 0);
                })
                processF.start(true);

                const processPM = camProcess.previewM = new CameraRecordProcess(cam.name, cam.preview, 1);
                processPM.addListener(CameraRecordProcessEventTypes.FAIL, () => {
                    // При ошибке переконнект к камере
                    logger.info(`[${cam.name}]::${MediaRecord.F_PREVIEW_SMALL}] Reconnect...`);
                    processPM.removeAllListeners();
                    processPM.dispose();
                    this.removeProcess(cam.name, 1);
                })
            }
        }
    }

    private removeProcess(name: string, type: number): void {
        if (this._recordProcesses.has(name)) {
            let camProcess: CamProcesses = this._recordProcesses.get(name);
            if (camProcess) {
                switch (type) {
                    case 0: {
                        camProcess.full.dispose();
                        break;
                    }
                    case 1: {
                        camProcess.previewM.dispose();
                        break;
                    }
                    /*case 2: {
                        camProcess.previewH.dispose();
                        break;
                    }*/
                }
                if (!camProcess.full && !camProcess.previewM/* && !camProcess.previewH*/) this._recordProcesses.delete(name);
            } else this._recordProcesses.delete(name);
        }
    }

    public dispose(): void {
        this._recordProcesses.forEach((v, k, map) => {
            if (v) {
                v.dispose();
                v = null;
            }
        });
        this._recordProcesses = null;
    }
}