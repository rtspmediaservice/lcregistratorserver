import * as express from "express";
import * as errorHandler from "errorhandler";
import * as bodyParser from "body-parser";
import * as path from "path";
import * as http from "http";

import { logger } from "./logger";
import { Settings } from "./settings";
import { RtspServer } from "./rtspServer/rtspServer";
import { CentralApi } from "./centralapi";
import { CService } from "./centralService";
import { TranslatorApi } from "./translatorApi";

export class App {

    private _expressApp: express.Application;
    public get expressApp(): express.Application {
        return this._expressApp;
    }

    private _httpService: http.Server;
    public get httpService(): http.Server {
        return this._httpService;
    }

    private _rtspService: RtspServer;
    public get rtspService(): RtspServer {
        return this._rtspService;
    }

    private _cService: CService;

    static PUBLIC: string = "public";

    static publicDir: string;

    constructor() {

        process.on('uncaughtException', (e) => {
            logger.error(e.message);
        });

        App.publicDir = `${__dirname}${path.sep}${App.PUBLIC}`;

        Settings.read((err) => {
            if (err) return logger.error("Error reading \"config.json\"");
        });

        this._expressApp = express();
        this._expressApp.set("port", Settings.server.port);

        // Добавление разрешенных заголовков
        this._expressApp.use(function (req, res, next) {
            res.header("Access-Control-Allow-Origin", "*");
            res.header("Access-Control-Allow-Methods", "GET,POST,PUT,DELETE,OPTIONS");
            res.header("Access-Control-Allow-Headers", "content-type, content-length, token, accept");
            res.header("Access-Control-Max-Age", "86400");
            if (req.method === "OPTIONS") {
                res.end();
            } else {
                next();
            }
        });

        this._expressApp.use(errorHandler());

        // Задание парсинга тела запроса
        //this._expressApp.use(bodyParser.json());
        //this._expressApp.use(bodyParser.urlencoded({ extended: true }));
        this._expressApp.use(express.static(`${__dirname}${path.sep}public`));

        this._httpService = http.createServer(this._expressApp);

        this._httpService.addListener("close", () => {
            logger.error(`Http server is stopped.`);
        });
        this._httpService.addListener("error", (err: Error) => {
            logger.error(`Http server is stopped. ${err}`);
        });

        //start our server
        this._httpService.listen(this._expressApp.get("port") || 8999, () => {
            logger.info(`Приложение запущено на http://localhost:${this._expressApp.get("port")} в ${this._expressApp.get("env")} mode`);
            logger.info("Для остановки нажмите CTRL-C\n");
            this.run();
        });
    }

    public run(): void {
        this._rtspService = new RtspServer();
        this._rtspService.run();
        
        CentralApi.createDivision(() => {
            this._cService = new CService(this._httpService);
            this._cService.initialize();
            console.log("Service running");
        });
    }
}

export const app = new App();