import * as fs from "fs";
import * as path from "path";
import { logger } from "./logger";

const uuidv1 = require('uuid/v1');
const IP = require("ip");

export interface IH264Quality {
    preset?: string;
    bitrate?: {
        min: string;
        max: string;
        buf: string;
        pass: number;
    }
}

export interface IOutputSettings {
    folder?: string;
    segmentTime?: number;
    quality?: IH264Quality;
    capacity?: string;
}

export interface ICameraSource {
    src: string;
    output: IOutputSettings;
}

export interface IDivisionSettings {
    name: string;
}

export interface ICameraSettings {
    name?: string;
    preview?: ICameraSource;
    full?: ICameraSource;
}

export interface IServerSettings {
    port: string;
    host: string;
}

export interface ICentralServerSettings {
    address: string;
    wsAddress: string;
}

export interface ITranslatorServerSettings {
    address: string;
}

export interface IFFMPEGSettings {
    ffmpegPath: string;
    ffprobePath: string;
    flvtoolPath: string;
}

export interface ISettingsData {
    uuid?: string;
    centralServer: ICentralServerSettings;
    translatorServer: ITranslatorServerSettings;
    server: IServerSettings;
    cameras: Array<ICameraSettings>;
    ffmpeg: IFFMPEGSettings;
    division?: IDivisionSettings;
}

class MainSettings {

    private _ip: string = "";
    public get ip(): string { return this._ip; }

    private _data: ISettingsData;
    public get data(): ISettingsData { return this._data; }

    public get uuid(): string { return this._data.uuid; }

    public get server(): IServerSettings { return this._data.server; }

    public get cameras(): Array<ICameraSettings> { return this._data.cameras; }

    public get ffmpeg(): IFFMPEGSettings { return this._data.ffmpeg; }

    public get centralServer(): ICentralServerSettings { return this._data.centralServer; }

    public get translatorServer(): ITranslatorServerSettings { return this._data.translatorServer; }

    public get division(): IDivisionSettings { return this._data.division; }

    private _camList: string[];
    public camList(): string[] { return this._camList; }

    /**
     * Чтение базовых настроек.
     */
    constructor() {
        this._ip = IP.address();
    }

    public read(cb: (err?: NodeJS.ErrnoException) => void): void {
        const data = fs.readFileSync('config.json', 'utf8');
        try {
            this._data = JSON.parse(data);
            this.generateUUID();
            this._camList = [];
            for (const cam of this._data.cameras) {
                this._camList.push(cam.name);
            }
        } catch (e) {
            return cb(e);
        }
        return cb();
    }

    private generateUUID(): void {
        if (!this._data.uuid) {
            this._data.uuid = uuidv1();
            fs.writeFileSync("config.json", JSON.stringify(this._data));
        }
    }
}

export let Settings = new MainSettings();
