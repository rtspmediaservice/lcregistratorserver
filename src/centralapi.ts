import * as httpRequest from "request";
import { Settings } from "./settings";
import { logger } from "./logger";

export class CentralApi {

    static getOptions(): any {

        return {
            body: {
                data: {
                    uuid: Settings.uuid,
                    name: Settings.division.name,
                    address: Settings.translatorServer.address,
                    //address: `${Settings.server.host}/${Settings.server.port}`,
                    camList: Settings.camList()
                }
            },
            json: true
        }
    }

    static createDivision(cb: Function): void {
        const options = CentralApi.getOptions();
        options.url = `http://${Settings.centralServer.address}/api/createdivision/`;
        httpRequest.post(options, (err, response, body) => {
            if (err) {
                logger.error(`[centralapi::createdivision] ${err}`);
                setTimeout(() => {
                    this.createDivision(cb);
                }, 5000);
            } else if (cb) cb();
        });
    }
}