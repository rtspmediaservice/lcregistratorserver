import * as express from "express";
import * as path from "path";
import * as fs from "fs";

const mkdirRequrs = require('mkdir-recursive');

const getSize = require('get-folder-size');

export interface IMakeDirResult {}

export const mkdirp = function (dir: string): Promise<IMakeDirResult> {
    return new Promise<IMakeDirResult>((resolve, reject) => {
        mkdirRequrs.mkdir(dir, (err) => {
            if (err) { if (err.message.indexOf('EEXIST') !== -1) resolve(); else reject(err)}
            else resolve();
          });
    });
}

interface IGetFilesSortedByCDateOptions {
    reverse?: boolean
}

export const getFilesSortedByCDate = function (dir: string, callback: (files: Array<string>) => void, options?: IGetFilesSortedByCDateOptions): void {
    let files: Array<string>;
    fs.readdir(dir, (err, result) => {
        if (err) throw err;
        files = result;

        files.sort((a: string, b: string) => {
            let sa: number;
            let sb: number;
            try {
                sa = fs.statSync(dir + a).mtime.getTime();
            } catch (e) { delete files[files.indexOf(a)]; }
            try {
                sb = fs.statSync(dir + b).mtime.getTime();
            } catch (e) { delete files[files.indexOf(b)] }
            let dif: number = options && options.reverse == true ? sa - sb : sb - sa;
            return dif;
        });
        if (callback) callback(files);
    });
}

export const getDirectorySize = function (dir: string, callback: Function): void {
    getSize(dir, (err: any, size: number) => {
        if (err) { throw err; }
        if (callback) callback(size);
    });
}

export const getBytesByStr = function (str: string): number {
    let result: number = 0;
    try {
        if (str && str.length > 0) {
            if (str.length > 1 && str.indexOf("K") == str.length - 1 || str.indexOf("k") == str.length - 1) {
                result = parseFloat(str.substring(0, str.length - 1)) * 1000;
            } else if (str.length > 1 && (str.indexOf("M") == str.length - 1 || str.indexOf("m") == str.length - 1)) {
                result = parseFloat(str.substring(0, str.length - 1)) * 1000000
            } else if (str.length > 1 && str.indexOf("G") == str.length - 1 || str.indexOf("g") == str.length - 1) {
                result = parseFloat(str.substring(0, str.length - 1)) * 1000000000;
            } else if (str.length > 1 && str.indexOf("T") == str.length - 1 || str.indexOf("t") == str.length - 1) {
                result = parseFloat(str.substring(0, str.length - 1)) * 1000000000000;
            } else result = parseFloat(str);
        }
    } catch (e) { }
    return result;
}

export const sendFile = function (filePath: string, fileName: string, response: express.Response): void {
    fs.exists(filePath, function (exists) {
        if (exists) {
            // Content-type is very interesting part that guarantee that
            // Web browser will handle response in an appropriate manner.
            response.writeHead(200, {
                "Content-Type": "application/octet-stream",
                "Content-Disposition": "attachment; filename=" + fileName
            });
            fs.createReadStream(filePath).pipe(response);
        } else {
            response.writeHead(400, { "Content-Type": "text/plain" });
            response.end("ERROR File does not exist.");
        }
    });
}