import * as http from "http";
import * as WebSocket from "ws";
import { Settings } from "./settings";
import { logger } from "./logger";
import { RtspServer, CamProcesses } from "./rtspServer/rtspServer";

export class CService {

    public static RT_INIT: string = "init";
    public static RT_PLAY_PREVIEW: string = "play-m";
    public static RT_PLAY_HIRES: string = "play-h";
    public static RT_STOP_PREVIEW: string = "stop-m";
    public static RT_STOP_HIRES: string = "stop-h";

    private static RECONNECT_TIMEOUT: number = 5000;

    private _client: WebSocket;

    private _reconnectTmr: NodeJS.Timer;

    constructor(private _httpService: http.Server) { }

    initialize() {
        this.createCli();
    }
    
    createCli() {
        this._client = new WebSocket("ws://" + Settings.centralServer.wsAddress + "/", []);

        this._client.on("message", (data) => {
            this.execCmd(String(data));
        });
        this._client.on("error", (e: any) => {
            logger.info(`[WS]::error ${e}`);
        });
        this._client.on("open", () => {
            this.init();
        });
        this._client.on("close", () => {
            logger.info(`[WS]::close`);
            this.stopAll();
            this.reconnect();
        });
    }

    private disposeCli(): void {
        if (this._client) {
            this._client.terminate();
            this._client = null;
        }
    }

    private reconnect(): void {
        this.stopReconnectTmr();
        this._reconnectTmr = setTimeout(() => {
            this.disposeCli();
            this.createCli();
        }, CService.RECONNECT_TIMEOUT);
    }

    private stopReconnectTmr(): void {
        if (this._reconnectTmr) {
            clearTimeout(this._reconnectTmr);
            this._reconnectTmr = null;
        }
    }

    private execCmd(data: string): void {
        if (!data) return;
        let cmd: any;
        try {
            cmd = JSON.parse(data);
        } catch (e) {
            logger.error("bad command");
            return;
        }

        switch (cmd.action) {
            case CService.RT_PLAY_PREVIEW: {
                this.play(cmd.data, true);
                break;
            }
            case CService.RT_PLAY_HIRES: {
                this.play(cmd.data);
                break;
            }
            case CService.RT_STOP_PREVIEW: {
                this.stop(cmd.data, true);
                break;
            }
            case CService.RT_STOP_HIRES: {
                this.stop(cmd.data);
                break;
            }
        }
    }

    private play(data: IRTMPCommand, preview: boolean = false) {
        const cams = data.cams;
        logger.info(`[RT]::#play ${preview ? 'preview' : 'hires'} [${cams.toString()}]`);
        RtspServer.instance.play(cams, preview);
    }

    private stopAll() {
        logger.info("[RT]::#stopAll");
        RtspServer.instance.stopAll();
    }

    private stop(data: IRTMPCommand, preview: boolean = false) {
        const cams = data.cams;
        logger.info(`[RT]::#stop ${preview ? 'preview' : 'hires'} [${cams.toString()}]`);
        RtspServer.instance.stop(cams, preview);
    }

    private init() {
        logger.info(`[WS]::init`);
        const command: ICommand<IRegistratorInit> = { action: CService.RT_INIT, data: { uuid: Settings.uuid } };
        this._client.send(JSON.stringify(command), (err) => {
            if (err) logger.error(`[WS]::init error. ${err.message}`);
        });
    }
}

export interface ICommand<T> {
    action: string;
    data: T;
}

export interface IRegistratorInit {
    uuid: string;
}

export interface IRTMPCommand {
    cams: Array<string>;
}