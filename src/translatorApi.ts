import * as httpRequest from "request";
import { Settings } from "./settings";

export class TranslatorApi {

    static deleteFile(file: string, cb: Function): void {
        try {
            const url = `http://${Settings.translatorServer.address}/api/deletefile`;
            httpRequest.post(url, {
                body: {
                    path: file
                },
                json: true
            }, (err, response, body) => {
                if (err) cb(err);
                else if (cb) cb();
            });
        } catch (error) {
            cb(error);
        }
    }
}